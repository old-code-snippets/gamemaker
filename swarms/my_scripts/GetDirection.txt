{
  /*
    GetDirection (x1, y1, x2, y2);
    Gets the closest direction of the eight (R,U,L,D, + diagonals)
  */

  dx = argument2 - argument0;
  dy = argument3 - argument1;

  if (dx >= 0) {
    if (dy >= 0) {
      if (dx >= (2*dy)) {
        // RIGHT
        dir 0;
      } else if (dy >= (2*dx)) {
        // DOWN
        dir 6;
      } else {
        // RIGHT/DOWN
        dir 7;
      }
    } else {
      if (dx >= -(2*dy)) {
        // RIGHT
        dir 0;
      } else if (-dy >= (2*dx)) {
        // UP
        dir 2;
      } else {
        // RIGHT/UP
        dir 1;
      }
    }
  } else {
    if (dy >= 0) {
      if (-dx >= (2*dy)) {
        // LEFT
        dir 4;
      } else if (dy >= -(2*dx)) {
        // DOWN
        dir 6;
      } else {
        // LEFT/DOWN
        dir 5;
      }
    } else {
      if (-dx >= -(2*dy)) {
        // LEFT
        dir 4;
      } else if (-dy >= -(2*dx)) {
        // UP
        dir 2;
      } else {
        // LEFT/UP
        dir 3;
      }
    }
  }
  return dir;
}
