
///////////////
// DOOR NODE //
///////////////

Separate each Area.

- index (integer) : index in "Nodes" array
- opened (boolean) : indicate if opened or closed

sprite size : ((CellSize - epsilon) * (CellSize - epsilon))

===========================================================================================================

LEFT-CLICK EVENT :
==================

// Create an Arc extremity (if end of Arc => create Arc)
if (obj_editor.ArcBegin)
{
	// origin point of arc
	obj_editor.ArcOrigin = index;	// index from "Door" object
	obj_editor.ArcBegin = 0;	// prepare for the second extremity of the Arc
}
else
{
	// end point of arc
	obj_editor.ArcEnd = index;
	obj_editor.ArcBegin = 1;

	// local saves to speed up process
	localNbArcs = obj_editor.ArcsNb;
	localArcOrigin = obj_editor.ArcOrigin;
	localArcEnd = obj_editor.ArcEnd;

	// check if arc already exists
	localArcExists = 0;
	for (i=0; (i < local_nb_arcs) && (!localArcExists); i+=1)
	{
		if (obj_editor.Arcs[i,0] = localArcOrigin)
		{
			if (obj_editor.Arcs[i,1] = localArcEnd)
			{
				localArcExists = 1;
			}
		}
	}

	// if arc doesn't already exist, add it in Arcs array
	if (!localArcExists)
	{
		obj_editor.Arcs[obj_editor.ArcsNb,0] = obj_editor.ArcOrigin;
		obj_editor.Arcs[obj_editor.ArcsNb,1] = obj_editor.ArcEnd;

		x1 = (obj_editor.Nodes[obj_editor.ArcOrigin]).x;
		x2 = (obj_editor.Nodes[obj_editor.ArcOrigin]).y;
		y1 = (obj_editor.Nodes[obj_editor.ArcEnd]).x;
		y2 = (obj_editor.Nodes[obj_editor.ArcEnd]).y;
		obj_editor.ArcsNb += 1;

		// if not directed, need to create the other way arc
		if (!obj_editor.ArcDirected)
		{
			obj_editor.Arcs[obj_editor.ArcsNb,0] = obj_editor.ArcEnd;
			obj_editor.Arcs[obj_editor.ArcsNb,1] = obj_editor.ArcOrigin;
			obj_editor.ArcsNb += 1;
		}

		// add the Arc in the "representations list"
		obj_editor.VisualArcsType[obj_editor.VisualArcsNb] = obj_editor.ArcDirected;
		obj_editor.VisualArcsOrigin[obj_editor.VisualArcsNb,0] = (obj_editor.Nodes[obj_editor.ArcOrigin]).x;
		obj_editor.VisualArcsOrigin[obj_editor.VisualArcsNb,1] = (obj_editor.Nodes[obj_editor.ArcOrigin]).y;
		obj_editor.VisualArcsEnd[obj_editor.VisualArcsNb,0] = (obj_editor.Nodes[obj_editor.ArcEnd]).x;
		obj_editor.VisualArcsEnd[obj_editor.VisualArcsNb,1] = (obj_editor.Nodes[obj_editor.ArcEnd]).y;
		obj_editor.VisualArcsNb += 1;
	}
}

===========================================================================================================

RIGHT-CLICK EVENT :
===================

// Switch between opened/closed status of a Door
if (opened)
{
	opened = false;
	sprite_index = spr_door_closed;
}
else
{
	opened = true;
	sprite_index = spr_door_opened;
}

===========================================================================================================
