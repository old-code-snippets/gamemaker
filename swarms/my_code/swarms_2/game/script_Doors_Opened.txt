{
	/* Doors_Opened
		=> indicates if the path from "iDoor" to "iArea" is "opened"
		 (path going from iDoor to iArea in iFloydNext, only has 1 in iDoorOpened equivalents)

		- Argument0 : FloydNext matrix (next Node in path to Area)
		- Argument1 : DoorOpened array (opened/closed status of Door)
		- Argument0 : index of Door
		- Argument1 : index of Area
	*/

	lOK = true;

	lFloydNext = obj_game.FloydNext;
	lDoorOpened = obj_game.DoorOpened;

	lNextNode = Argument0;
	lArea = Argument1;

	while (lNextNode != -1)
	{
		if (!lDoorOpened[lNextNode])
		{
			lNextNode = -1;
			lOK = false;
		}
		else
		{
			lNextNode = lFloydNext[lNextNode][lArea];
		}
	}

	return lOK;
}
