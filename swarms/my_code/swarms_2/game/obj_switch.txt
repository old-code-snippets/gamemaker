
///////////////////
// AREA SWITCHES //
///////////////////

===========================================================================================================

obj_switch : Area Switch
Object to track in which Area the player is.

"area" is set at the Map loading

sprite size : ((CellSize - epsilon) * (CellSize - epsilon))
(idem obj_door ?)

===========================================================================================================
