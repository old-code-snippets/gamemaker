{
	/* Load_Map
		Loads a Map from a file.
		Loaded data is :
			- Blocks positions in the Map Grid
			- Doors positions in the Map Grid
			- Specials positions in the Map Grid
			- Spawns positions in the Map Grid
			- ... ?
	*/
/*
Ask the user a file to open, then reads data in it.
(file should contains : blocks, doors, specials, spawns, ... ?)

=> could also read a "full map", containing the Arcs, Switches, etc
(well, only Arcs would be better, and everything is then recomputed ...)

=> NEED TO CLEAR EVERYTHING WE HAVE BUILT BEFORE LOADING !!!!
*/

	lOK = false;

	lCellSize = obj_editor.CellSize;

	lFileName = "";
// !!!! TO DO : change extension ... !!!!
	//lFilter = "map|*.map"
	lFilter = "map|*.txt"

	// ask user to select a file to open
	get_open_filename (lFilter, lFileName);
	if (lFileName != "")
	{
		// open file
		lFileID = file_text_open_read (lFileName);
// !!!! TO DO : check if opening went ok !!!!

		// reinit the Editor's data
		Re_Init ();

		// ...
/*
file_text_read_string (fileid)
file_text_read_real (fileid)
file_text_readln (fileid)

STRINGS : look at page 103 !!!!
*/

		// header data ... ???? (anything ?)
		//...

		tmpStr = file_text_read_string (lFileID);

// !!!! TO DO : may need to reverse H & V ???? !!!!
		CellsVNb = 0;
		CellsHNb = string_length (tmpStr);

		i = CellsVNb;

		tmpX = (lCellSize / 2);
		tmpY = (lCellSize / 2);

		// get the Map composition line by line
		while (!file_text_eof (lFileID))
		{
			for (j=0; j < CellsHNb; ++j)
			{
				tmpChar = string_char_at (tmpStr, j);

				// ' ' character => empty Cell
// !!!! TO DO : OK ???? characters in single quotes ???? !!!!
				if (tmpChar = ' ')
				{
					continue;
				}

				//create corresponding object instance
				switch (tmpChar)
				{
// !!!! TO DO : OK ???? characters in single quotes ???? !!!!
				case 'x':	// Block

					instance_create (tmpX, tmpY, obj_block);
					break;

				case 'd':	// Door
					instance_create (tmpX, tmpY, obj_door);
					break;

				case 's':	// Special
					instance_create (tmpX, tmpY, obj_special);
					break;

				case 'e':	// Ennemy Spawn
					instance_create (tmpX, tmpY, obj_spawn);
					break;

				default:
					break;
				}
				tmpY += lCellSize;
			}
			tmpX += lCellSize;
			++i;
			tmpStr = file_text_read_string (lFileID);
		}
		CellsVNb = i;





		// close file
		file_text_close (lFileID);

		// Update Editor's variables
		// size of Map
		obj_editor.CellsHNb = CellsVNb;	// number of Columns
		obj_editor.CellsVNb = CellsHNb;	// number of Rows

		lOK = true;
	}
	return lOK;
}
