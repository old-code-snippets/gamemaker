{
	/* Save_Map
		Saves the Map data in a file.
		Saved data is :
			- CellSize

			- CellsHNb, CellsVNb

			- Blocks : number + positions
				(BlocksNb + (Blocks[i]).x,y)
			- Doors : numbers + positions
				(DoorsNb + (Nodes[i]).x,y)
			- Specials : number + positions
				(SpecialsNb + (Nodes[DoorsNb + i]).x,y)

			???? - DoorAreas ????

			- FloydNext

			???? - FloydDistance ????

			- NextDoor

			- NewArea

			- Switches : number + positions
				(SwitchesNb + (Switches[i]).x,y + related Area ... ?)

			- Ennemy Spawns : number + positions

	*/

	lOK = false;

	lFileName = "";
// !!!! TO DO : change extension ... !!!!
	//lFilter = "map|*.map"
	lFilter = "map|*.txt"

	// ask user to select a file to save
	get_save_filename (lFilter, lFileName);
	if (lFileName != "")
	{
		// open file
		lFileID = file_text_open_write (lFileName);

/*
file_text_write_string (fileid,str)
 Writes the string to the file with the given file id.

file_text_write_real (fileid,x)
 Write the real value to the file with thid.

file_text_writeln (fileid)
 Write a newline character to the file.
*/

////////////////////////////////////////

		// CellSize
		...

		// CellsHNb, CellsVNb
		...

		// Blocks : number + positions
		lBlocksNb = obj_editor.BlocksNb;
		file_text_write_real (lFileID, lBlocksNb);
		file_text_writeln (lFileID);
		for (i=0; i < lBlocksNb; ++i)
		{
			file_text_write_real (lFileID, (obj_editor.Blocks[i]).x);
// !!!! TO DO : ok to use a ' ' to separate the 2 values ???? (or easier with "writeln" ????) !!!!
			file_text_write_string (lFileID, " ");
			file_text_write_real (lFileID, (obj_editor.Blocks[i]).y);
			file_text_writeln (lFileID);
		}

		// Doors : number + positions
		lDoorsNb = obj_editor.DoorsNb;
		file_text_write_real (lFileID, lDoorsNb);
		file_text_writeln (lFileID);
		for (i=0; i < lDoorsNb; ++i)
		{
			// index needed ????
			//...
			file_text_write_real (lFileID, (obj_editor.Nodes[i]).x);
			file_text_write_string (lFileID, " ");
			file_text_write_real (lFileID, (obj_editor.Nodes[i]).y);
			file_text_writeln (lFileID);
		}

		// Specials : number + positions
		lSpecialsNb = obj_editor.SpecialsNb;
		file_text_write_real (lFileID, lSpecialsNb);
		file_text_writeln (lFileID);
		for (i=0; i < lSpecialsNb; ++i)
		{
			// index needed ????
			//...
			file_text_write_real (lFileID, (obj_editor.Nodes[lDoorsNb + i]).x);
			file_text_write_string (lFileID, " ");
			file_text_write_real (lFileID, (obj_editor.Nodes[lDoorsNb + i]).y);
			file_text_writeln (lFileID);
		}

		// FloydNext
// !!!! TO DO : need to check if never have an Area as first parameter !!!!
		// => can save the sub matrix ((DoorsNb + SpecialsNb) X (AreasNb))
		lTmpNb = obj_editor.DoorsNb + obj_editor.SpecialsNb;
//// !!!! TO DO : can remove this, already wrote it before !!!!
		file_text_write_real (lFileID, lTmpNb);
		file_text_write_string (lFileID, " ");
//// !!!!
		lAreasNb = obj_editor.AreasNb;
		file_text_write_real (lFileID, lAreasNb);
		file_text_writeln (lFileID);
		for (i=0; i < lTmpNb; ++i)
		{
			for (j=0; j < lAreasNb; ++j)
			{
				file_text_write_real (lFileID, obj_editor.FloydNext[i][lTmpNb + j]);
				file_text_write_string (lFileID, " ");
			}
			file_text_writeln (lFileID);
		}

		// NextDoor
		// number of Doors per Area to Area,
		// followed by list of Doors indices ordered depending on their distance
// !!!! TO DO : OK ???? !!!!
		for (i=0; i < lAreasNb; ++i)
		{
			for (j=0; j < lAreasNb; ++j)
			{
				lAreaDoorsNb = obj_editor.AreaDoorsNb[i];
				file_text_write_real (lFileID, lAreaDoorsNb);
				file_text_writeln (lFileID);
				for (k=0; k < lAreaDoorsNb; ++k)
				{
					file_text_write_real (lFileID, obj_editor.NextDoor[i][j][k]);
					file_text_write_string (lFileID, " ");
				}
				file_text_writeln (lFileID);
			}
		}

		// NewArea
		for (i=0; i < lDoorsNb; ++i)
		{
			for (j=0; j < lAreasNb; ++j)
			{
				file_text_write_real (lFileID, obj_editor.NewArea[i][j]);
				file_text_write_string (lFileID, " ");
			}
			file_text_writeln (lFileID);
		}

		// Switches : number + positions + area
////////////////
// !!!! TO DO : stopped here ... !!!!
// "Squared Area Switches"
SwitchesNb = instance_number (obj_switch_square);
for (i=0; i < SwitchesNb; ++i)
{
	tmpSwitch = instance_find (obj_switch_square, i);
	Switches[i] = tmpSwitch.id;
	tmpSwitch.index = i;
}

// get all "Horizontal Area Switches" instances
tmpSwitchesNb = instance_number (obj_switch_hor);
for (i=0; i < tmpSwitchesNb; ++i)
{
	tmpSwitch = instance_find (obj_switch_hor, i);
	Switches[SwitchesNb + i] = tmpSwitch.id;
	tmpSwitch.index = SwitchesNb + i;
}
SwitchesNb += tmpSwitchesNb;

// get all "Vertical Area Switches" instances
tmpSwitchesNb = instance_number (obj_switch_ver);
for (i=0; i < tmpSwitchesNb; ++i)
{
	tmpSwitch = instance_find (obj_switch_ver, i);
	Switches[SwitchesNb + i] = tmpSwitch.id;
	tmpSwitch.index = SwitchesNb + i;
}
SwitchesNb += tmpSwitchesNb;

//  set the Area index (corresponding to the Area Centers) for the Area Switches
for (i=0; i < SwitchesNb; ++i)
{
// !!!! TO DO : can be removed ... (?) !!!!
	//SwitchAreas[i] = GridAreaIndex[(Switches[i]).x div CellSize][(Switches[i]).y div CellSize]
// !!!!

	(Switches[i]).area = GridAreaIndex[(Switches[i]).x div CellSize][(Switches[i]).y div CellSize]
}
////////////////




// !!!! TO DO : seems useless, but need to check !!!!
// ???? - DoorAreas ????
// ???? - FloydDistance ????



			- Ennemy Spawns : number + positions

////////////////////////////////////////


		// close file
		file_text_close (lFileID);

		lOK = true;
	}
	return lOK;
}
