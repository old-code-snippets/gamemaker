
- rename new/script/Load_Full_Map to script/Load_Map

- rename script/Load_Map to script/Load_Tile_Map

- rename new/script/Save_Full_Map to script/Save_Map

- rename script/Save_Map to script/Export_Map
	( => and should call Generate at this point ? )

- change "Generate" button to "Export" button

- make 2 buttons to load maps :
	- "Load Tile Map"
		=> To load a tile map (txt file ?) only containing the Blocks, Doors, Specials, etc.
		( current "Load" button )
	- "Load Map"
		=> To load a map containing Arcs
		( NEW )
		( doesn't load the Flloyd data, as will have to be regenerated )

- when creating Arcs, detect if Arc in oppposite direction already exists, if so, create a visual 2-way Arc !



