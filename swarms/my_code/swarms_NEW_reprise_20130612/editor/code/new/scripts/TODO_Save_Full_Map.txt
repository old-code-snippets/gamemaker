{
    /*
      Save_Map()
        Saves the Map data in a file.
        Saved data is :
          - CellsHNb, CellsVNb
          - Blocks : number + positions
          - Doors : number + index & positions
          - Specials : number + index & positions
          - Arcs : number + begin positions + end positions
+ create VisualArcs from Arcs data !
          - Spawns : number + positions + area
          - Hero : position + area
    */

// !!!! TODO : change format of file !!!!
// => possible to use hexa ?
// => & possible to set values on same line ? ( so not using "new lines" to separate values ... )
// ( also, no need to write strings such as "[Cells]", etc. )

    lOK = false;

    lFileName = "";
// !!!! TO DO : change extension ... !!!!
    //lFilter = "map|*.map"
    lFilter = "map|*.txt"

    // ask user to select a file to save
    get_save_filename( lFilter, lFileName );
    if ( lFileName != "" )
    {
        // open file
        lFileID = file_text_open_write( lFileName );
// !!!! TO DO : should check if opening went OK !!!!

        lCellSize = obj_editor.CellSize;


        ////////////////////////////////////////////////////////////////
        // [Cells]
        // CellsHNb
        // CellsVNb
        ////////////////////////////////////////////////////////////////

// !!!! TODO : useless - to remove !!!!
        // [Cells]
        file_text_write_string( lFileID, "[Cells]" );
        file_text_writeln( lFileID );

        // CellsHNb
        file_text_write_real( lFileID, obj_editor.CellsHNb );
        file_text_writeln( lFileID );

        // CellsVNb
        file_text_write_real( lFileID, obj_editor.CellsVNb );
        file_text_writeln( lFileID );

// !!!! TODO : useless - to remove !!!!
        file_text_writeln( lFileID );


        ////////////////////////////////////////////////////////////////
        // [Blocks]
        // BlocksNb
        // x # for each Block
        // y # for each Block
        ////////////////////////////////////////////////////////////////

// !!!! TODO : useless - to remove !!!!
        // [Blocks]
        file_text_write_string( lFileID, "[Blocks]" );
        file_text_writeln( lFileID );

        // BlocksNb
        lBlocksNb = obj_editor.BlocksNb;
        file_text_write_real( lFileID, lBlocksNb );
        file_text_writeln( lFileID );

        for ( i = 0; i < lBlocksNb; i += 1 )
        {
            // x
            file_text_write_real( lFileID, ( obj_editor.Blocks[ i ] ).x div lCellSize );
            file_text_writeln( lFileID );

            // y
            file_text_write_real( lFileID, ( obj_editor.Blocks[ i ] ).y div lCellSize );
            file_text_writeln( lFileID );
        }

// !!!! TODO : useless - to remove !!!!
        file_text_writeln( lFileID );


        ////////////////////////////////////////////////////////////////
        // [Doors]
        // DoorsNb
        // index # for each Door
        // x # for each Door
        // y # for each Door
// !!!! TODO : add state of Door ( opened / closed ) !!!!
        ////////////////////////////////////////////////////////////////

// !!!! TODO : useless - to remove !!!!
        // [Doors]
        file_text_write_string( lFileID, "[Doors]" );
        file_text_writeln( lFileID );

        // DoorsNb
        lDoorsNb = obj_editor.DoorsNb;
        file_text_write_real( lFileID, lDoorsNb );
        file_text_writeln( lFileID );

        for ( i = 0; i < lDoorsNb; i += 1 )
        {
            // index
            file_text_write_real( lFileID, ( obj_editor.Nodes[ i ] ).index );
            file_text_writeln( lFileID );

            // x
            file_text_write_real( lFileID, ( obj_editor.Nodes[ i ] ).x div lCellSize );
            file_text_writeln( lFileID );

            // y
            file_text_write_real( lFileID, ( obj_editor.Nodes[ i ] ).y div lCellSize );
            file_text_writeln( lFileID );
        }

// !!!! TODO : useless - to remove !!!!
        file_text_writeln( lFileID );


        ////////////////////////////////////////////////////////////////
        // [Specials]
        // SpecialsNb
        // index # for each Special
        // x # for each Special
        // y # for each Special
        ////////////////////////////////////////////////////////////////

// !!!! TODO : useless - to remove !!!!
        // [Specials]
        file_text_write_string( lFileID, "[Specials]" );
        file_text_writeln( lFileID );

        // SpecialsNb
        lSpecialsNb = obj_editor.SpecialsNb;
        file_text_write_real( lFileID, lSpecialsNb );
        file_text_writeln( lFileID );

        for ( i = 0; i < lSpecialsNb; i += 1 )
        {
            // index
            file_text_write_real( lFileID, ( obj_editor.Nodes[ lDoorsNb + i ] ).index );
            file_text_writeln( lFileID );

            // x
            file_text_write_real( lFileID, ( obj_editor.Nodes[ lDoorsNb + i ] ).x div lCellSize );
            file_text_writeln( lFileID );

            // y
            file_text_write_real( lFileID, ( obj_editor.Nodes[ lDoorsNb + i ] ).y div lCellSize );
            file_text_writeln( lFileID );
        }

// !!!! TODO : useless - to remove !!!!
        file_text_writeln( lFileID );


ARCS + VISUAL ARCS !!!!
        ////////////////////////////////////////////////////////////////
        // [Arcs]
        // ArcsNb
        // Arcs[ i, 0-1 ]
        ////////////////////////////////////////////////////////////////
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
obj_editor.Arcs[obj_editor.ArcsNb,0] = obj_editor.ArcOrigin;
obj_editor.Arcs[obj_editor.ArcsNb,1] = obj_editor.ArcEnd;
obj_editor.ArcsNb += 1;

// !!!! TODO : useless - to remove !!!!
        // [Arcs]
        file_text_write_string( lFileID, "[Arcs]" );
        file_text_writeln( lFileID );

        // ArcsNb
        lArcsNb = obj_editor.ArcsNb;
        file_text_write_real( lFileID, lArcsNb );
        file_text_writeln( lFileID );

        // Arcs[ i, 0-1 ]
        for ( i = 0; i < lTmpNb; i += 1 )
        {
            for ( j = 0; j < lAreasNb; j += 1 )
            {
                file_text_write_real( lFileID, obj_editor.FloydNext[ i, lTmpNb + j ] );
                file_text_writeln( lFileID );
            }
            file_text_writeln( lFileID );
        }

// !!!! TODO : useless - to remove !!!!
        file_text_writeln( lFileID );
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        ////////////////////////////////////////////////////////////////
        // [Spawns]
        // SpawnsNb
        // x # for each Spawn
        // y # for each Spawn
        // area # for each Spawn
        ////////////////////////////////////////////////////////////////

// !!!! TODO : useless - to remove !!!!
        // [Spawns]
        file_text_write_string( lFileID, "[Spawns]" );
        file_text_writeln( lFileID );

        // SpawnsNb
        lSpawnsNb = obj_editor.SpawnsNb;
        file_text_write_real( lFileID, lSpawnsNb );
        file_text_writeln( lFileID );

        for ( i = 0; i < lSpawnsNb; i += 1 )
        {
            // x
            file_text_write_real( lFileID, ( obj_editor.Spawns[ i ] ).x div lCellSize );
            file_text_writeln( lFileID );

            // y
            file_text_write_real( lFileID, ( obj_editor.Spawns[ i ] ).y div lCellSize );
            file_text_writeln( lFileID );

            // area
            file_text_write_real( lFileID, ( obj_editor.Spawns[ i ] ).area );
            file_text_writeln( lFileID );
        }

// !!!! TODO : useless - to remove !!!!
        file_text_writeln( lFileID );


        ////////////////////////////////////////////////////////////////
        // [Hero]
        // x
        // y
        // area
        ////////////////////////////////////////////////////////////////

// !!!! TODO : useless - to remove !!!!
        // [Hero]
        file_text_write_string( lFileID, "[Hero]" );
        file_text_writeln( lFileID );

        // x
        file_text_write_real( lFileID, ( obj_editor.Hero ).x div lCellSize );
        file_text_writeln( lFileID );

        // y
        file_text_write_real( lFileID, ( obj_editor.Hero ).y div lCellSize );
        file_text_writeln( lFileID );

        // area
        file_text_write_real( lFileID, ( obj_editor.Hero ).area );
        file_text_writeln( lFileID );

// !!!! TODO : useless - to remove !!!!
        file_text_writeln( lFileID );


        ////////////////////////////////////////////////////////////////

// !!!! ???? TODO : ANYTHING ELSE ? ???? !!!!
//...

        // close file
        file_text_close( lFileID );

        lOK = true;
    }
    return lOK;
}
