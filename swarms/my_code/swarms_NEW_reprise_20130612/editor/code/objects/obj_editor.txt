
////////////////
// OBJ_EDITOR //
////////////////


!!!! Room in GAMEMAKER must have the biggest accepted size !!!!
!!!! Room must be filled with Blocks (no empty cells) !!!!

Generates & keeps the data, draws the created arcs & the current one if any
(& displays info (1way or 2way arc, ... ))

- not visible
- not solid


=> create the Map depending on the Room created in GameMaker,
	and then, if user clicks on "Load" button, clear all and load Map from file


===========================================================================================================

CREATION EVENT :
================

// define the constants
// !!!! TO DO : define CONSTS !!!! => in GM (constants -> Add)
INF = 1000000	// big number to represent INF
////

// initialise the variables
Init ();

// "sleep" a while to make sure everything gets created
alarm[0] = 60;	// ~2 seconds

===========================================================================================================

ALARM0 EVENT :
==============

// check if have one & only one Hero => ~ "acceptable map"
if (instance_number (obj_hero) = 1)
{
	////////////////////////////////////////////////////////////////////////////////
	// Blocks, Nodes, Create_Areas (GridAreaIndex, DoorAreas, DoorAreasNb)
	////////////////////////////////////////////////////////////////////////////////

	// get all "obj_block" instances & store them in "Blocks" array
	// => BlocksNb, Blocks[0, BlocksNb - 1]
	BlocksNb = instance_number (obj_block);
	for (i=0; i < BlocksNb; i+=1)
	{
		Blocks[i] = instance_find (obj_block, i);
	}

	// get all "obj_door" instances & store them in "Nodes" array
	// => DoorsNb, Nodes[0, DoorsNb - 1]
	DoorsNb = instance_number (obj_door);
	for (i=0; i < DoorsNb; i+=1)
	{
		Nodes[i] = instance_find (obj_door, i);
		(Nodes[i]).index = i;
		(Nodes[i]).opened = true;	// open by default
	}

	// get all "obj_special" instances & store them in "Nodes" array, after the "Doors"
	// => SpecialsNb, Nodes[DoorsNb, DoorsNb + SpecialsNb - 1]
	SpecialsNb = instance_number (obj_special);
	for (i=0; i < SpecialsNb; i+=1)
	{
		Nodes[DoorsNb + i] = instance_find (obj_special, i);
		(Nodes[DoorsNb + i]).index = i + DoorsNb;
	}

	// create the "Area Centers"
	// => GridAreaIndex, DoorAreas, DoorAreasNb
	Create_Areas ();

	// get all "obj_area" instances & store them in "Nodes" array, after the "Specials"
	// => AreasNb, NodesNb, Nodes[DoorsNb + SpecialsNb, NodesNb - 1]
	AreasNb = instance_number (obj_area);
	AreaFirstIndex = DoorsNb + SpecialsNb;	// save offset of first Area for later use
	for (i=0; i < AreasNb; i+=1)
	{
		Nodes[DoorsNb + SpecialsNb + i] = instance_find (obj_area, i);
		(Nodes[DoorsNb + SpecialsNb + i]).index = i + DoorsNb + SpecialsNb;
	}
	NodesNb = DoorsNb + SpecialsNb + AreasNb;

	// ready to generate the Map
	OK = true;

}	// end if (instance_number (obj_hero) = 1)


===========================================================================================================

ALARM1 EVENT :
==============
// Triggered when click on "Generate Button"

if (OK = true)
{
	////////////////////////////////////////////////////////////////////////////////
	// ArcsLength
	////////////////////////////////////////////////////////////////////////////////

	// init : set all the values to "infinity"
	for (i=0; i < NodesNb; i+=1)
	{
		for (j=0; j < NodesNb; j+=1)
		{
			ArcsLength[i,j] = INF;
		}
	}
	// compute all the Arcs Lengths
	for (i=0; i < ArcsNb; i+=1)
	{
		x1 = (Nodes[Arcs[i,0]]).x;
		x2 = (Nodes[Arcs[i,0]]).y;
		y1 = (Nodes[Arcs[i,1]]).x;
		y2 = (Nodes[Arcs[i,1]]).y;
		ArcsLength[Arcs[i,0],Arcs[i,1]] = point_distance (x1, y1, x2, y2);
	}

	////////////////////////////////////////////////////////////////////////////////
	// FloydDistance & FloydNext
	////////////////////////////////////////////////////////////////////////////////

	// init : prepare the adjacent matrix
	for (i=0; i < NodesNb; i+=1)
	{
		for (j=0; j < NodesNb; j+=1)
		{
			FloydDistance[i,j] = ArcsLength[i,j];

			if (ArcsLength[i,j] = INF)
			{
				FloydNext[i,j] = -1;
			}
			else
			{
				FloydNext[i,j] = j;
			}
		}
	}
	// generate the Floyd matrices
	// (=> Floyd[j,k] = min (Floyd[j,k], Floyd[j,i] + Floyd[i,k]))
	for (i=0; i < NodesNb; i+=1) {

		for (j=0; j < NodesNb; j+=1) {

			tmpVal = FloydDistance[j,i];

//////////////// begin
// !!!! TO DO : TO TEST !!!!
/*			for (k=0; k < NodesNb; k+=1) {
////////
				if (tmpVal = INF) {
					tmpVal2 = INF;
				} else {
					tmpVal2 = tmpVal + FloydDistance[i,k];
				}
////////
// !!!! TO DO : no need to continue in "for" if = INF ???? !!!!
////////
//				if (tmpVal2 < FloydDistance[j,k]) {
				if ((j != k) and (tmpVal2 < FloydDistance[j,k])) {
////////
					FloydDistance[j,k] = tmpVal2;
					FloydNext[j,k] = i;
				}
			}
*/
//////////////// middle
			for (k=0; k < NodesNb; k+=1) {
				if (k != j) {
					if (tmpVal = INF) {
						tmpVal2 = INF;
					} else {
						tmpVal2 = tmpVal + FloydDistance[i,k];
					}
					if (tmpVal2 < FloydDistance[j,k]) {
						FloydDistance[j,k] = tmpVal2;
						FloydNext[j,k] = i;
					}
				}
			}
//////////////// end
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// (AreaNodes?, AreaNodesNb?), AreaDoors, AreaDoorsNb
	////////////////////////////////////////////////////////////////////////////////

	// set to 0 the Length for Arcs going to an Area Node,
	//  & create the lists of Nodes inside each Area
	//  (those for which Length is different than "infinity")
	for (i=0; i < AreasNb; i+=1)
	{
// !!!! TO DO : + remove AreaNodes & AreaNodesNb ... ???? !!!!
		// number of nodes in the Area
		//AreaNodesNb[i] = 0;
		// number of doors in the Area
		AreaDoorsNb[i] = 0;

		for (j=0; j < NodesNb; j+=1)
		{
			if (ArcsLength[j,AreaFirstIndex + i] != nb_max)
			{
				// set Distance from Node to Area to 0
				ArcsLength[j,AreaFirstIndex + i] = 0;

				// add in Area's List of Nodes
				//AreaNodes[i,AreaNodesNb[i]] = j;
				//(AreaNodesNb[i])+=1;

				// if its a door, add in Area's List of Doors
				if (j < DoorsNb)
				{
					AreaDoors[i,AreaDoorsNb[i]] = j;
					(AreaDoorsNb[i])+=1;
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// NextDoorIndex, NextDoor
	////////////////////////////////////////////////////////////////////////////////

	// sort the Lists of Doors depending on their Distance to each Area
	//  (using a Bubble sort algorithm)
// !!!! TO DO : check if ok !!!!
	tmpIndex = 0;
	for (i=0; i < AreasNb; i+=1)
	{
		for (j=0; j < AreasNb; j+=1)
		{
			tmpExchanges = true;
			tmpN = AreaDoorsNb[i];

			NextDoorIndex[i,j] = tmpIndex;

			// copy the indices in the new structure
			for (k=0; k < tmpN; k+=1)
			{
				NextDoor[tmpIndex,k] = AreaDoors[i,k];
			}

			do
			{
				tmpN += 1;		// make loop smaller each time.
				tmpExchanges = false;	// assume this is last pass over array
				for (k=0; k < tmpN; k+=1)
				{
					//if (FloydDistance[NextDoor[tmpIndex,k],j] > FloydDistance[NextDoor[tmpIndex,k+1],j])
					{
						tmpVal = NextDoor[tmpIndex,k];
						NextDoor[tmpIndex,k] = NextDoor[tmpIndex,k+1];
						NextDoor[tmpIndex,k+1] = tmpVal;
						tmpExchanges = true;	// after an exchange, must look again 
					}
				}
			} until (tmpExchanges = false);
			tmpIndex += 1;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// NewArea
	////////////////////////////////////////////////////////////////////////////////

	// determine for each Arc the Area transition it does
	//  => the only common Area for the 2 Arc's extremities
	for (i=0; i < DoorsNb; i+=1)
	{
		for (j=0; j < AreasNb; j+=1)
		{
			tmpArcOrigin = i;
			tmpArcEnd = FloydNext[i,j];

			NewArea[i,j] = -1;
			for (k=0; k < DoorAreasNb[tmpArcOrigin]; k+=1)
			{
				for (l=0; l < DoorAreasNb[tmpArcEnd]; l+=1)
				{
					if (DoorAreas[tmpArcOrigin,k] = DoorAreas[tmpArcEnd,l])
					{
						NewArea[i,j] = DoorAreas[tmpArcOrigin,k];

						// force exit of loops
						k = DoorAreasNb[tmpArcOrigin];
						l = DoorAreasNb[tmpArcEnd];
					}
				}
			}
// !!!! TO DO : do something when ERROR ???? !!!!
/*
			if (NewArea[i,j] = -1)
			{
				// ERROR : WRONG MAP !
				// => if here, means there is more than one graph !
			}
*/
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// Switches
	////////////////////////////////////////////////////////////////////////////////

	// get all the "obj_switch" instances, store them in the "Switches" array,
	//  & set their corresponding Area index
	SwitchesNb = instance_number (obj_switch);
	for (i=0; i < SwitchesNb; i+=1)
	{
		Switches[i] = instance_find (obj_switch, i);
// !!!! TO DO : NEEDED ???? !!!!
		(Switches[i]).index = i;
		(Switches[i]).area = GridAreaIndex[(Switches[i]).x div CellSize,(Switches[i]).y div CellSize]
	}

	////////////////////////////////////////////////////////////////////////////////
	// Spawns
	////////////////////////////////////////////////////////////////////////////////

	// get all the "obj_spawn" instances, store them in the "Spawns" array,
	//  & set their corresponding Area index
	SpawnsNb = instance_number (obj_spawn);
	for (i=0; i < SpawnsNb; i+=1)
	{
		Spawns[i] = instance_find (obj_spawn, i);
// !!!! TO DO : NEEDED ???? !!!!
		(Spawns[i]).index = i;
		(Spawns[i]).area = GridAreaIndex[(Spawns[i]).x div CellSize,(Spawns[i]).y div CellSize]
	}

	////////////////////////////////////////////////////////////////////////////////
	// Hero
	////////////////////////////////////////////////////////////////////////////////

	// set the Hero corresponding Area index
	Hero = instance_find (obj_hero, 0);
	(Hero).area = GridAreaIndex[(Hero).x div CellSize,(Hero).y div CellSize]

}	// end if (OK = true)

===========================================================================================================

ALARM2 EVENT :
==============
// Triggered when click on "Load Button"
// => Clears the data, then load new data from file
//  (if loading fails, will lose all data)

Init ();

if (Load_Map () = true)
	OK = true;

// Trigger Alarm0 to generate the objects & data structures
alarm[0] = 60;

===========================================================================================================

ALARM3 EVENT :
==============
// Triggered when click on "Save Button"
// => Saves the data into a file

// !!!! TO DO : should make some checks before saving ... ???? !!!!
//?Check_Map ();

Save_Map ();

===========================================================================================================

ESC KEYBOARD BUTTON PRESSED :
=============================
// cancel the currently created line (if any)
ArcBegin = 1;

===========================================================================================================

DRAW EVENT :
============

// draw the created arcs
for (i=0; i < VisualArcsNb; i+=1)
{
	if (VisualArcsType[i])		// directed arc (1-way)
	{
		draw_set_color(c_blue);
	}
	else				// not directed arc (2-ways)
	{
		draw_set_color(c_green);
	}
	draw_line (VisualArcsOrigin[i,0], VisualArcsOrigin[i,1],
				VisualArcsEnd[i,0], VisualArcsEnd[i,1]);
}

// draw the arc currently in construction if any
if (!ArcBegin)
{
	draw_set_color(c_red);
	draw_line ((Nodes[ArcOrigin]).x, (Nodes[ArcOrigin]).y, global_mouse.x, global_mouse.y);
}

===========================================================================================================
