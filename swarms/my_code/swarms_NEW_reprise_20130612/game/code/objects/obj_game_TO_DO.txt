

!!!!

NEED DIFFERENT OBJECTS DEPENDING ON ROOMS !!!!
- obj_intro in INTRO_ROOM
- obj_game in GAME_ROOM
?- obj_outro in OUTRO_ROOM
- ...?

!!!!



//////////////
// OBJ_GAME //
//////////////

!!!!
?=> PERSISTENT OBJECT
(not visible, not solid)
!!!!

/*
// used variables
DoorsNb
SpecialsNb
AreasNb
NodesNb (= DoorsNb + SpecialsNb)
*/

// used structures
FloydNext[NodesNb][AreasNb]
NewArea[DoorsNb][AreasNb]
AreaDoorsNb[AreasNb]
Nodes[NodesNb].x
Nodes[NodesNb].y
DoorOpened[DoorsNb]


===========================================================================================================

"game_status" handled values :
INTRO		: intro of the game ... (animation ?)
MENUS		: game menus ... (main, options, ...)
LEVEL_IN	: loading + init a level
IN_GAME		: in a level
PAUSED		: paused
LEVEL_OUT	: cleaning + saving a level
(!!!! TO DO : save ? or password ? or ... ? ? ? ?)
QUIT		: leaving/ended



// !!!! TO DO : need to define these CONSTANTS :
INTRO = 0;
MENUS = 1;
LEVEL_IN = 2;
IN_GAME = 3;
PAUSED = 4;
LEVEL_OUT = 5;
QUIT = 6;


===========================================================================================================

- need to set "strCurrentMapFile" for each new level/map

===========================================================================================================

CREATION EVENT :
================

//////// BEGIN
CellSize = 32;
//////// END


// !!!! TO DO : should start with an Intro, followed by the Menus
//game_status = INTRO;
game_status = LEVEL_IN;

// !!!! TO DO : change format !!!!
strCurrentMapFile = "map1.txt";


===========================================================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
� First, in the current room (if any) all instances get a room-end event. Next the non-persistent instances are removed (no destroy event is generated!).
� Next, for the new room the persistent instances from the previous room are added.
� All new instances are created and their creation events are executed (if the room is not persistent or has not been visited before).
� When this is the first room, for all instances the game-start event is generated.
� Now the room creation code is executed.
� Finally, all instances get a room-start event.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!
INSTEAD, could use events :

- room's creation code ... ????
(of ... through GM interface, not in game)

for objects :
- game start
- game end
- room start
- room end

!!!!
????	can use a single ROOM, & restart it for every level
		(with Load_Map in Room' creation event)
OR
	can have a different ROOM for each level ...
	(better to avoid no ?)

IN BOTH CASES, need "special rooms" :
- INTRO room (=> room_first)
- MENUS room
- TRANSITION room (between levels ?)
- OUTRO/QUIT room (=> room_last ?)
!!!!


!!!!
!!!! CHECK IF FUNCTIONS ON PAGE 179-180 ARE AVAILABLE IN NOT REGISTERED MODE !!!!
!!!!



!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!
!!!! WATCH FOR VIEWS !!!!
(look at page 153)

- activate & deactivate instances !!!!
	(instance_deactivate_region, instance_activate_object, ...)
!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!



STEP EVENT :
============
// depending on "game_status"
switch (game_status)
{
case INTRO:
	//...
	break;

case MENUS:
	//...
	break;

case LEVEL_IN:
	if (!Load_Map (strCurrentMapFile))
	{
// !!!! TO DO : handle ERRORS !!!!
		ERROR !!!!
	}
	game_status = IN_GAME;
	break;
/*
// !!!! TO DO : needed ???? !!!!
case IN_GAME:
	// ... ?
	break;
*/
case PAUSED:
	break;

case LEVEL_OUT:
	Clear_Map();
	strCurrentMapFile = ... ????

// !!!! TO DO : OK ???? HERE ???? !!!!
	//game_status = LEVEL_IN;
	break;

case QUIT:
	break;
}






===========================================================================================================
