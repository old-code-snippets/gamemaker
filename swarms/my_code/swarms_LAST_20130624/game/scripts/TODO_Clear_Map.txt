{
    /*
      Clear_Map()
        Clears all the Map data.
        Cleared data is :
          - CellsHNb, CellsVNb
          - Blocks
          - Doors
          - Specials
          - FloydNext
          - NextDoorIndex
          - NextDoor
          - AreaDoorsNb
          - NewArea
          - Switches
          - Spawns
          - Hero

// !!!! TO DO : seems useless, but need to check !!!!
          ???? - DoorAreas ????
          ???? - FloydDistance ????
    */

    // Cells
    CellsHNb = 0;
    CellsVNb = 0;

    // Blocks
    lTmpNb = obj_game.BlocksNb;
    for ( i = 0; i < lTmpNb; i+=1 )
    {
        obj_game.Blocks[ i ] = -1;
    }
    with( obj_block ) instance_destroy();
    obj_game.BlocksNb = 0;

    // Nodes ( Doors & Specials )
    lTmpNb = obj_game.NodesNb;
    lDoorsNb = obj_game.DoorsNb;
    for ( i = 0; i < lTmpNb; i+=1 )
    {
        obj_game.Nodes[ i ] = -1;
    }
    with( obj_door ) instance_destroy();
    with( obj_special ) instance_destroy();
    obj_game.NodesNb = 0;
    obj_game.DoorsNb = 0;
    obj_game.SpecialsNb = 0;

    // Areas
    lAreasNb = obj_game.AreasNb;
    obj_game.AreasNb = 0;

    // FloydNext
// !!!! TODO : useless => could be removed !!!!
    for ( i = 0; i < lTmpNb; i += 1 )
    {
        for ( j = 0; j < lAreasNb; j += 1 )
        {
            obj_game.FloydNext[ i ][ j ] = -1;
        }
    }

    // NextDoor
// !!!! TODO : useless => could be removed !!!!
    for ( i = 0; i < lAreasNb; i += 1 )
    {
        for ( j = 0; j < lAreasNb; j += 1 )
        {
            tmpAreaDoorsNb = obj_game.AreaDoorsNb[ i ];
            obj_game.AreaDoorsNb[ i ] = 0;
            for ( k = 0; k < tmpAreaDoorsNb; k += 1 )
            {
                obj_game.NextDoor[ obj_game.NextDoorIndex[ i, j ], k ] = -1;
            }
            obj_game.NextDoorIndex[ i, j ] = -1;
        }
    }

    // NewArea
// !!!! TODO : useless => could be removed !!!!
    for ( i = 0; i < lDoorsNb; i+= 1 )
    {
        for ( j = 0; j < lAreasNb; j += 1 )
        {
            obj_game.NewArea[ i, j ] = -1;
        }
    }

    // Switches
    with( obj_switch ) instance_destroy();
    obj_game.SwitchesNb = 0;

    // Spawns
    with( obj_spawn ) instance_destroy();
    obj_game.SpawnsNb = 0;

    // Hero
    with( obj_hero ) instance_destroy();
    obj_game.Hero = 0;


// !!!! ???? TODO : ANYTHING ELSE ? ???? !!!!
//...

}
