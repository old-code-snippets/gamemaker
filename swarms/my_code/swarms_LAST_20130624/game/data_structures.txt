
//////////////////////////
// GAME DATA STRUCTURES //
//////////////////////////

=> data structures kept in "obj_game"

===========================================================================================================

Variables :
===========
?CellSize : size in pixel of a Cell from the Map Grid (width = height)
?CellsHNb : horizontal number of Cells in the Map Grid
?CellsVNb : vertical number of Cells in the Map Grid
?INF : big number to represent infinity ...

NodesNb : number of Nodes (Doors + Specials + Areas)
DoorsNb : number of Doors
SpecialsNb : number of Specials
AreasNb : number of Areas
AreaFirstIndex : index in the Nodes array of the first Area (= DoorsNb + SpecialsNb)

ArcsNb : number of Arcs (all 1-way Arcs)
VisualArcsNb : number of Arcs to display (< ArcsNb, as considering 2-ways Arcs)
ArcBegin : indicate that the next click will start an Arc
ArcDirected : indicate if the next Arc created will be 1-way (directed) or 2-ways

===========================================================================================================

Created at ROOM CREATION :
==========================

(load from file)
- Nodes[DoorsNb + SpecialsNb + AreasNb] : ID of Doors and Special Nodes (not with Areas)

- NextDoor[AreasNb][AreasNb][AreaDoorsNb[]] : ordered list of "Doors" with shortest path from "Area i" to "Area j"

- FloydNext[DoorsNb + SpecialsNb][AreasNb] : index of next Node in path from "Node i" to "Area j"

// !!!! TO DO : NEEDED ???? !!!!
- FloydDistance[DoorsNb + SpecialsNb][AreasNb] : total distance from "Node i" to "Area j"
	(without last step from last node to Area, as =0)
// !!!!

- DoorOpened[DoorsNb] : booleans to indicate if "Door i" is currently opened

===========================================================================================================

Scripts :
=========
- Doors_Opened (iFloydNext, iDoorOpened, iDoor, iArea) :
	indicates if all the Doors on the path from iDoor to iArea are opened or not

===========================================================================================================
