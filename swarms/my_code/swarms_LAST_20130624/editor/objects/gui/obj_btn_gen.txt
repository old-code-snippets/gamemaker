
LEFT MOUSE PRESSED
==================

- Execute a piece of code:

// create "Area Switches"
//  => create 4 Area Switches around each Door
//   (colliding ones will be fused in their Collision Event

tmpOffset = (obj_editor.CellSize / 2);

// delete already created switches
with (obj_switch) instance_destroy();

// for each "Door"
for (i=0; i < obj_editor.DoorsNb; i+=1)
{
    // Get door's position
    lx = (obj_editor.Nodes[i]).x;
    ly = (obj_editor.Nodes[i]).y;

    // Create 4 Area Switches around it
    instance_create (lx, ly - tmpOffset, obj_switch);// above
    instance_create (lx + tmpOffset, ly, obj_switch);// right
    instance_create (lx, ly + tmpOffset, obj_switch);// below
    instance_create (lx - tmpOffset, ly, obj_switch);// left
}

// set a Timer to trigger an Alarm in "obj_editor"
// (to let time for the Area Switches to collide each other)
obj_editor.alarm[1] = 60;// ~2 seconds
