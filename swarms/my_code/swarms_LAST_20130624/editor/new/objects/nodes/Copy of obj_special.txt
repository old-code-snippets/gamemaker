
LEFT MOUSE PRESSED
==================

- Execute a piece of code:

if ( !obj_editor.bScriptRunning )
{
    obj_editor.bScriptRunning = true;

    // Create an Arc extremity ( if end of Arc => create Arc )
    if ( obj_editor.ArcBegin )
    {
        // origin point of arc
        obj_editor.ArcOrigin = index; // index from "Door" object
        obj_editor.ArcBegin = 0; // prepare for the second extremity of the Arc
    }
    else
    {
        // end point of arc
        obj_editor.ArcEnd = index;
        obj_editor.ArcBegin = 1;

        // local saves to speed up process
        lNbArcs = obj_editor.ArcsNb;
        lArcOrigin = obj_editor.ArcOrigin;
        lArcEnd = obj_editor.ArcEnd;

        // check if arc already exists
        lArcExists = 0;
        for ( i = 0; ( i < lNbArcs ) && ( !lArcExists ); i += 1 )
        {
            if ( obj_editor.Arcs[ i, 0 ] = lArcOrigin )
            {
                if ( obj_editor.Arcs[ i, 1 ] = lArcEnd )
                {
                    lArcExists = 1;
                }
            }
        }

        // if arc doesn't already exist, add it in Arcs array
        if ( !lArcExists )
        {
            obj_editor.Arcs[ obj_editor.ArcsNb, 0 ] = obj_editor.ArcOrigin;
            obj_editor.Arcs[ obj_editor.ArcsNb, 1 ] = obj_editor.ArcEnd;
            obj_editor.ArcsNb += 1;

            // if not directed, need to create the other way arc
            if ( !obj_editor.ArcDirected )
            {
                obj_editor.Arcs[ obj_editor.ArcsNb, 0 ] = obj_editor.ArcEnd;
                obj_editor.Arcs[ obj_editor.ArcsNb, 1 ] = obj_editor.ArcOrigin;
                obj_editor.ArcsNb += 1;
            }

            // add the Arc in the "representations list"
            obj_editor.VisualArcsType[ obj_editor.VisualArcsNb ] = obj_editor.ArcDirected;
            obj_editor.VisualArcsOrigin[ obj_editor.VisualArcsNb, 0 ] = ( obj_editor.Nodes[ obj_editor.ArcOrigin ] ).x;
            obj_editor.VisualArcsOrigin[ obj_editor.VisualArcsNb, 1 ] = ( obj_editor.Nodes[ obj_editor.ArcOrigin ] ).y;
            obj_editor.VisualArcsEnd[ obj_editor.VisualArcsNb, 0 ] = ( obj_editor.Nodes[ obj_editor.ArcEnd ] ).x;
            obj_editor.VisualArcsEnd[ obj_editor.VisualArcsNb, 1 ] = ( obj_editor.Nodes[ obj_editor.ArcEnd ] ).y;
            obj_editor.VisualArcsNb += 1;
        }
    }

    obj_editor.bScriptRunning = false;

} // end if ( !obj_editor.bScriptRunning )
