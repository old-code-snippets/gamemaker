
LEFT MOUSE PRESSED
==================

- Execute a piece of code:

if ( !obj_editor.bScriptRunning )
{
    obj_editor.bScriptRunning = true;

    // trigger alarm in obj_editor
    obj_editor.alarm[ 1 ] = 1;

} // end if ( !obj_editor.bScriptRunning )
