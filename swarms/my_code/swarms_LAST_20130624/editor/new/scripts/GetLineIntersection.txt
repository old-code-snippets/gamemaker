{
    /*
      GetLineIntersection( Argument0, Argument1, Argument2, Argument3,
                           Argument4, Argument5, Argument6, Argument7 )

        - Argument0 : Line 1 Start X
        - Argument1 : Line 1 Start Y
        - Argument2 : Line 1 End X
        - Argument3 : Line 1 End Y
        - Argument4 : Line 2 Start X
        - Argument5 : Line 2 Start Y
        - Argument6 : Line 2 End X
        - Argument7 : Line 2 End Y
    */

    s1X = Argument2 - Argument0;
    s1Y = Argument3 - Argument1;

    s2X = Argument6 - Argument4;
    s2Y = Argument7 - Argument5;

    // check if segments can intercept
    if ( s1X >= 0 ) // Argument0 <= Argument2
    {
        minX1 = Argument0;
        maxX1 = Argument2;
    }
    else // Argument0 > Argument2
    {
        minX1 = Argument2;
        maxX1 = Argument0;
    }
    if ( s1Y >= 0 ) // Argument1 <= Argument3
    {
        minY1 = Argument1;
        maxY1 = Argument3;
    }
    else // Argument1 > Argument3
    {
        minY1 = Argument3;
        maxY1 = Argument1;
    }

    if ( s2X >= 0 ) // Argument4 <= Argument6
    {
        minX2 = Argument4;
        maxX2 = Argument6;
    }
    else // Argument4 > Argument6
    {
        minX2 = Argument6;
        maxX2 = Argument4;
    }
    if ( s2Y >= 0 ) // Argument5 <= Argument7
    {
        minY2 = Argument5;
        maxY2 = Argument7;
    }
    else // Argument5 > Argument7
    {
        minY2 = Argument7;
        maxY2 = Argument5;
    }

    if ( ( maxX1 < minX2 ) || ( minX1 > maxX2 ) || ( maxY1 < minY2 ) || ( minY1 > maxY2 ) )
    {
        return false;
    }


    // determine if interception
    dX = Argument0 - Argument4;
    dY = Argument1 - Argument5;

    denum = ( -s2X * s1Y ) + ( s1X * s2Y );
    if ( denum == 0 )
    {
        return false;
    }

    s = ( ( -s1Y * dX ) + ( s1X * dY ) ) / denum;

    t = ( ( s2X * dY ) - ( s2Y * dX ) ) / denum;

    if ( ( s >= 0 ) && ( s <= 1 ) && ( t >= 0 ) && ( t <= 1 ) )
    {
        // Collision detected
        return true;
    }
    return false; // No collision
}
