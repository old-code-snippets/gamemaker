{
    /*
      Variables used in the script :
        - Argument0 : current Area Index
        - Argument1 : Horizontal Index in Map Grid
        - Argument2 : Vertical Index in Map Grid
    */

    AreaIndex = argument0;
    HIndex = argument1;
    VIndex = argument2;

    tmpCellSize = obj_editor.CellSize;

    // make sure we are in the grid
    if ( ( HIndex >= 0 ) && ( HIndex < obj_editor.CellsHNb )
        && ( VIndex >= 0 ) && ( VIndex < obj_editor.CellsVNb ) )
    {
        // check if don't already have an Area
        if ( obj_editor.GridAreaIndex[ HIndex, VIndex ] = -1 )
        {
            XPos = ( HIndex * tmpCellSize ) + ( 0.5 * tmpCellSize );
            YPos = ( VIndex * tmpCellSize ) + ( 0.5 * tmpCellSize );

            // check if Cell doesn't contain a Block
            if ( !position_meeting( XPos, YPos, obj_block ) )
            {
                // check if Cell doesn't contain a Door
                if ( !position_meeting( XPos, YPos, obj_door ) )
                {
                    // no need to handle cells with a Door,
                    // as they don't have any Switches in them
                    // ( and they have more than 1 Area )

////////
                    if ( position_meeting( XPos, YPos, obj_special ) )
                    {
                        // save Area index
                        tmpSpecialID = instance_nearest( XPos, YPos, obj_special );
                        tmpSpecialIndex = ( tmpSpecialID ).index;

                        obj_editor.DoorAreas[ tmpSpecialIndex, obj_editor.DoorAreasNb[ tmpSpecialIndex ] ] = AreaIndex;
                        obj_editor.DoorAreasNb[ tmpSpecialIndex ] += 1;
                    }
////////

                    // set Area for current Cell
                    obj_editor.GridAreaIndex[ HIndex, VIndex ] = AreaIndex;

                    // propagate to surrounding cells
// !!!! ???? TODO : why doing like this ? ???? !!!!
// => have problems when passing values ?
                    //Propagate( AreaIndex, HIndex + 1, VIndex );
                    //Propagate( AreaIndex, HIndex,     VIndex + 1 );
                    //Propagate( AreaIndex, HIndex - 1, VIndex );
                    //Propagate( AreaIndex, HIndex,     VIndex - 1 );
                    HIndex += 1;
                    Propagate( AreaIndex, HIndex, VIndex );
                    HIndex -= 1;
                    VIndex += 1;
                    Propagate( AreaIndex, HIndex, VIndex );
                    HIndex -= 1;
                    VIndex -= 1;
                    Propagate( AreaIndex, HIndex, VIndex );
                    HIndex += 1;
                    VIndex -= 1;
                    Propagate( AreaIndex, HIndex, VIndex );
                    VIndex += 1;
                }
                else
                {
// !!!! TO DO : check if OK !!!!
                    // save Area index for the found Door
                    tmpDoorID = instance_nearest( XPos, YPos, obj_door );
                    tmpDoorIndex = ( tmpDoorID ).index;

                    obj_editor.DoorAreas[ tmpDoorIndex, obj_editor.DoorAreasNb[ tmpDoorIndex ] ] = AreaIndex;
                    obj_editor.DoorAreasNb[ tmpDoorIndex ] += 1;
                }
            }
        }
    }
}
