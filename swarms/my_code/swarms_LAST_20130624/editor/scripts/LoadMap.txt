{
// !!!! TODO : rename to "LoadTileMap" !!!!
    /* LoadMap()
        Loads a Map from a file.
        ( should contain : Blocks, Doors, Specials, Spawns, Hero )

        Loaded data is :
          - Blocks positions in the Map Grid
          - Doors positions in the Map Grid
          - Specials positions in the Map Grid
          - Spawns positions in the Map Grid
          - Hero position in the Map Grid
    */

    lOK = false;

    lFileName = "";
// !!!! TO DO : change extension ... !!!!
    //lFilter = "map|*.map"
    lFilter = "map (.txt)|*.txt"

    // ask user to select a file to open
    lFileName = get_open_filename( lFilter, lFileName );
    if ( lFileName != "" )
    {
        // open file
        lFileID = file_text_open_read( lFileName );
// !!!! TO DO : check if opening went ok !!!!

        // init the Editor's data
        Init();

        // read the first line of the file
        tmpStr = file_text_read_string( lFileID );

        lCellsVNb = 0;
        lCellsHNb = string_length( tmpStr );

        lCellSize = obj_editor.CellSize;
        tmpY = -( lCellSize / 2 );
        i = 0; // row counter

        // get the Map composition line by line
        while ( !file_text_eof( lFileID ) )
        {
            tmpX = -( lCellSize / 2 );
            tmpY += lCellSize;

            for ( j = 1; j <= lCellsHNb; j += 1 )
            {
                tmpChar = string_char_at( tmpStr, j );
                tmpX += lCellSize;

                //create corresponding object instance
                switch( tmpChar )
                {
                case 'x': // Block
                    instance_create( tmpX, tmpY, obj_block );
                    break;

                case 'd': // Door
                    instance_create( tmpX, tmpY, obj_door );
                    break;

                case 's': // Special
                    instance_create( tmpX, tmpY, obj_special );
                    break;

                case 'e': // Ennemy Spawn
                    instance_create( tmpX, tmpY, obj_spawn );
                    break;

                case 'h': // Hero
                    instance_create( tmpX, tmpY, obj_hero );
                    break;

                default:
                    break;
                }
            }
            i+=1;
            tmpStr = file_text_readln( lFileID );
            tmpStr = file_text_read_string( lFileID );
        }
        lCellsVNb = i;

        // close file
        file_text_close( lFileID );

        // Update Editor's variables
        // size of Map
        obj_editor.CellsHNb = lCellsHNb; // number of Columns
        obj_editor.CellsVNb = lCellsVNb; // number of Rows

        lOK = true;
    }
    return lOK;
}