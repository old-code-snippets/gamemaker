
// !!!! TODO : to remove ... !!!!
game_width = 640;
game_height = 480;


// Constants
INF = 10000;

ACC = 0.25;
FRI = 0.5;
CONST_FACTOR = FRI / (FRI + ACC);

// !!!! ???? TODO : what value ? ???? !!!!
// ( constant ? variable depending on speed ? ... ? )
minDist = 1.0;

// !!!! TODO : need to recompute da and df when change maxSpeed !!!!
maxSpeed = 10.0;
//da = ( (maxSpeed / ACC) * ( (maxSpeed / ACC) + 1.0) / 2.0 ) * ACC;
da = maxSpeed * ( (maxSpeed / ACC) + 1.0) / 2.0 ;
//df = ( (maxSpeed / FRI) * ( (maxSpeed / FRI) - 1.0) / 2.0 ) * FRI ;
df = maxSpeed * ( (maxSpeed / FRI) - 1.0) / 2.0 ;


dir = 0;
oldDir = -1; // force different than targetDir at init

dist = 0.0;

posX = x;
posY = game_height - y;

speed = 0.0;
speedX = 0.0;
speedY = 0.0;



bNewMove = false;
bPush = false;

bAccel = false;
bFri = false;
accelX = ACC;
accelY = 0.0;
friX = FRI;
friY = 0.0;

bTarget = false;
targetDist = 0.0;
targetDir = 0;
oldTargetDir = -1; // force different than targetDir at init

bSlowDown = false;

// OK ????
stopDist = 0.0;
stopSpeed = 0.0;

targetX = 0.0;
targetY = 0.0;

