
=> absoluteTargetMove( x, y )

/*
absoluteTargetMove( x, y )
{
//    relativeTargetMove( x - posX, y - posY );
    relativeTargetMove( x - posX, (game_height - y) - posY );
}

relativeTargetMove( x, y )
{
    bNewMove = true;

    bTarget = true;
    deltaX = x;
    deltaY = y;

////
/*
// !!!! TODO : to remove (if using integers) !!!!
// => to "round" the value to 2 decimals
    deltaX = ( (int)( deltaX * 100 ) ) / 100.0;
    deltaY = ( (int)( deltaY * 100 ) ) / 100.0;
*/
////

    // default values, may be changed if distance is "too short"
    stopDist = df;
    stopSpeed = maxSpeed;
}
*/

--------

bNewMove = true;

bTarget = true;
deltaX = mouse_x - posX;
deltaY = (game_height - mouse_y) - posY;

////
/*
// !!!! TODO : to remove (if using integers) !!!!
// => to "round" the value to 2 decimals
deltaX = ( (int)( deltaX * 100 ) ) / 100.0;
deltaY = ( (int)( deltaY * 100 ) ) / 100.0;
*/
////

stopDist = df;
stopSpeed = maxSpeed;
