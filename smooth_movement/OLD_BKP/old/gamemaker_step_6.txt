
/*
!!!!

TODO :
where to use integers or double ?

for numbers close to 0 (or else) +> round !!!!
accelX, accelY, friX, friY, speed, ...

!!!!
*/


// new move
if ( bNewMove )
{
// !!!! ???? TODO : needed ? ???? !!!!
//?    bAccel = true;
//?    bFri = false;

    if ( !bTarget )
    {
        bNewMove = false;

// !!!! ???? TODO : something else to do ? ???? !!!!
// ...
    }
    else
    {
        // determine direction and distance to target
        deltaX = targetX - posX;
        deltaY = targetY - posY;
        if ( deltaX == 0 )
        {
            if ( deltaY == 0 )
            {
                targetDir = dir;
                targetDist = 0;
            }
            else if ( deltaY > 0 )
            {
                targetDir = 90;
                targetDist = deltaY;
            }
            else
            {
                targetDir = 270;
                targetDist = deltaY;
            }
        }
        else
        {
// !!!! TODO : find a way to avoid using "atan" !!!!
// !!!! ???? TODO : maybe no need to convert directions from radians to degrees ? ???? !!!!
// => can maybe do everything in radians ?
            targetDir = arctan( deltaY / deltaX );
            targetDir = round (targetDir * ( 180 / 3.141592 ));

            if ( deltaX > 0 )
            {
                if ( deltaY < 0 )
                {
                    targetDir = 360 + targetDir;
                }
            }
            else
            {
                targetDir = 180 + targetDir;
            }
            if ( targetDir == 360 )
            {
                targetDir = 0;
            }

            if ( deltaY == 0 )
            {
                targetDist = deltaX;
            }
            else
            {
                targetDist = sqrt( (deltaX * deltaX) + (deltaY * deltaY) );
            }
        }
// !!!! ???? TODO : OK ? ???? !!!!
        if ( targetDist <= minDist )
        {
            targetDist = 0;
        }
    }


    if ( targetDir == dir )
    {
        bNewMove = false;

        if ( (targetDist > 0) && (targetDist < da + df) )
        {
            stopSpeed = sqrt( ( FRI / (FRI + ACC) ) * ( (speed * speed) + ACC * ( (2 * targetDist) + speed ) ) );

            stopSpeed = ( (stopSpeed - speed) / ACC ) + 0.5;
            stopSpeed = floor( stopSpeed );
            stopSpeed += speed;
            stopSpeed *= ACC;

// !!!! TODO : check that stopDist is correctly initialised if not passing here !!!!
            stopDist = ( 1 / (2 * FRI) ) * stopSpeed;
            stopDist -= 0.5 * stopSpeed;
        }
    }

}


// if new target direction => determine new acceleration
if ( oldTargetDir != targetDir )
{
    oldTargetDir = targetDir;


// !!!! ???? TODO : multiply by ACC or FRI here, or when computing speed ? ???? !!!!
// => if here, less multiplications, but more variables
// (and useless computing of fri or acc if in other case)
/*
// !!!! ???? TODO : better to compute with this instead of cos/sin ? ???? !!!!
// (if not, useless ...)
    if ( bTarget )
    {
        accelX = ( deltaX * ACC ) / targetDist;
        accelY = ( deltaY * ACC ) / targetDist;
    }
    else
    {
*/
        tmpCos = cos ( targetDir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : what values to use ? ???? !!!!
        if ( abs( tmpCos ) < 0.01 )
        {
            tmpCos = 0.0;
        }
        else if ( tmpCos > 0.99 )
        {
            tmpCos = 1.0;
        }
        else if ( tmpCos < -0.99 )
        {
            tmpCos = -1.0;
        }

        tmpSin = sin ( targetDir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : what values to use ? ???? !!!!
        if ( abs( tmpSin ) < 0.01 )
        {
            tmpSin = 0;
        }
        else if ( tmpSin > 0.99 )
        {
            tmpSin = 1.0;
        }
        else if ( tmpSin < -0.99 )
        {
            tmpSin = -1.0;
        }

        accelX = tmpCos * ACC;
        accelY = tmpSin * ACC;

//    }


}

// if new direction => determine new friction
if ( oldDir != dir )
{
    oldDir = dir;

    if ( dir == targetDir )
    {
// !!!! ???? TODO : OK to use *(FRI/ACC) ? ???? !!!!
// => to avoid recomputing cos and sin ...
        friX = ( accelX / ACC ) * FRI;
        friY = ( accelY / ACC ) * FRI;
    }
    else
    {

        tmpCos = cos ( dir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : what values to use ? ???? !!!!
        if ( abs( tmpCos ) < 0.01 )
        {
            tmpCos = 0.0;
        }
        else if ( tmpCos > 0.99 )
        {
            tmpCos = 1.0;
        }
        else if ( tmpCos < -0.99 )
        {
            tmpCos = -1.0;
        }

        tmpSin = sin ( dir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : what values to use ? ???? !!!!
        if ( abs( tmpSin ) < 0.01 )
        {
            tmpSin = 0;
        }
        else if ( tmpSin > 0.99 )
        {
            tmpSin = 1.0;
        }
        else if ( tmpSin < -0.99 )
        {
            tmpSin = -1.0;
        }

        friX = tmpCos * FRI;
        friY = tmpSin * FRI;
    }

}



// !!!! TODO : "push" boolean to precise we're doing a "move" !!!!
if ( !bPush )
{


// !!!! ???? TODO : OK ? ???? !!!!
// => hack to avoid stopping before reaching target ...
// !!!! ???? TODO : should check if "bTarget" ? ???? !!!!
/*
if ( (speed == 0) && (targetDist > 0) )
{
    bAccel = true;
}
else
*/



    // !!!! TODO : check that only the case when "stopMove" !!!!
    // => if not, check that ok
    if ( stopDist == INF ) // "stopMove" => need to stop asap
    {
        // start slowing down asap
        bAccel = false;
        bFri = true;
    }
    else
    {
        if ( targetDist == 0 ) // reached target => stop
        {
//// 1 - BEGIN
// !!!! TODO : should slow down but maybe not stop !!!!
// => if speed is high, then it will be a sudden stop ... => not good !
/*
            if ( speed < ...? )
            {
*/
//// 1 - END
            bTarget = false;

            bAccel = false;
            bFri = false;

            speed = 0.0;
            speedX = 0.0;
            speedY = 0.0;

// !!!! ???? TODO : something else to do ? ???? !!!!
// ...

//// 1 - BEGIN
// !!!! ???? TODO : usefull ? ???? !!!!
/*
            }
            else
            {
                bAccel = false;
                bFri = true;
            }
*/
//// 1 - END

        }
        else if ( targetDist != INF ) // not in "startMove" case (not continuous movement)
        {

/*
            // reached "stopDist"
            if ( targetDist <= stopDist )
            {
                // directions are "close"
// !!!! ???? TODO : what angles interval ? ???? !!!!
//?                if ( (abs( dir - targetDir ) <= 45) || (abs( dir - targetDir ) >= 315) )
//?                if ( (abs( dir - targetDir ) <= 90) || (abs( dir - targetDir ) >= 270) )
                if ( (abs( dir - targetDir ) <= 135) || (abs( dir - targetDir ) >= 225) )
                {
                    // start slowing down
                    bAccel = false;
                    bFri = true;
                }
                else
                {
                    // accelerate in "different" direction AND start slowing down
                    bAccel = true;
                    bFri = true;
                }
            }
            else // "far" from target
            {
                // directions are "close"
// !!!! ???? TODO : what angles interval ? ???? !!!!
//?                if ( (abs( dir - targetDir ) <= 45) || (abs( dir - targetDir ) >= 315) )
//?                if ( (abs( dir - targetDir ) <= 90) || (abs( dir - targetDir ) >= 270) )
                if ( (abs( dir - targetDir ) <= 135) || (abs( dir - targetDir ) >= 225) )
                {
                    // accelerate
                    bAccel = true;
                    bFri = false;
                }
                else
                {
                    // accelerate in "different" direction AND start slowing down
                    bAccel = true;
                    bFri = true;
                }
            }
*/

            // if directions are "not close"
// !!!! ???? TODO : what angles interval ? ???? !!!!
//?            if ( (abs( dir - targetDir ) > 45) && (abs( dir - targetDir ) < 315) )
            if ( (abs( dir - targetDir ) > 90) && (abs( dir - targetDir ) < 270) )
//?            if ( (abs( dir - targetDir ) > 135) && (abs( dir - targetDir ) < 225) )
            {
toto =1;
// !!!! ???? TODO : better to only "break" or also "accelerate" ? ???? !!!!
                // accelerate in "different" direction AND start slowing down on "current" direction
                bAccel = true;
//?                bAccel = false;
                bFri = true;
            }
            else // directions are "close"
            {
                // reached "stopDist"
                if ( targetDist <= stopDist )
                {
toto =2;
                    // start slowing down
                    bAccel = false;
                    bFri = true;
                }
                else
                {
                    // accelerate (if not too fast already)
//////// FORWARD SPEED - BEGIN
/*
                    bAccel = true;
                    bFri = false;
*/
//////// FORWARD SPEED - MID
// !!!! ???? TODO : OK ? ???? !!!!
//                    if ( speed < stopSpeed )
                    {
toto =3;
                        bAccel = true;
                        bFri = false;
                    }
/*                    else
                    {
toto =4;
                        bAccel = false;
                        bFri = true;
                    }
                    */
//////// FORWARD SPEED - END
                }

            }


        }

    }



    // acceleration
    if ( bAccel )
    {

        speedX += accelX;
        speedY += accelY;

        speed = sqrt ( (speedX * speedX) + (speedY * speedY) );
// !!!! TODO : compare with "stopSpeed+-epsilon" !!!!
//        if ( speed == stopSpeed )
        if ( (speed < stopSpeed + epsilon) && (speed > stopSpeed - epsilon) )
        {
            bAccel = false;
//...
        }
        else if ( speed > stopSpeed )
        {
            bAccel = false;
//...

            if ( stopDist != INF )
            {
                speedX *= stopSpeed / speed;
                speedY *= stopSpeed / speed;
                speed = stopSpeed;
            }

        }

    }


    // friction
// !!!! ???? TODO : OK ? ???? !!!!
//    if ( bFri )
    if ( bFri && (speed > 0) )
    {

        // determine speedX
        if (sign( speedX - friX) != sign( speedX ) )
        {
            speedX = 0.0;
        }
        else
        {
            speedX -= friX;
        }

        // determine speedY
        if (sign( speedY - friY) != sign( speedY ) )
        {
            speedY = 0.0;
        }
        else
        {
            speedY -= friY;
        }


        // determine speed
        if ( speedX != 0 )
        {
            if ( speedY != 0 )
            {
                speed = sqrt ( (speedX * speedX) + (speedY * speedY) );
            }
            else
            {
//                speed = speedX;
                speed = abs( speedX );
            }
        }
        else
        {
            if ( speedY != 0 )
            {
//                speed = speedY;
                speed = abs( speedY );
            }
            else
            {
                speed = 0;
            }
        }

// !!!! TODO : compare with "0+-epsilon" !!!!
// and set speed to 0.0
//        if ( speed == 0 )
        if ( (speed < epsilon) && (speed > - epsilon) )
        {
////
            speed = 0.0;
            speedX = 0.0;
            speedY = 0.0;
////
            bFri = false;
//...
        }

    }

}
else // bPush
{
    // new speed => probably new dir => new move
    bNewMove = true;
    speed = sqrt( (speedX * speedX) + (speedY * speedY) );
}

//////// DIR - MID
// !!!! TODO : should avoid recomputing "dir" every time ... !!!!
//{

    if ( speedX == 0 )
    {
        if ( speedY == 0 )
        {
//            dir = 0;
            dir = targetDir;
        }
        else if ( speedY > 0 )
        {
            dir = 90;
        }
        else
        {
            dir = 270;
        }
    }
    else
    {
// !!!! TODO : find a way to avoid using "atan" !!!!
        dir = arctan( speedY / speedX );
        dir = round (dir * ( 180 / 3.141592 ));

        if ( speedX > 0 )
        {
            if ( speedY < 0 )
            {
                dir = 360 + dir;
            }
        }
        else
        {
            dir = 180 + dir;
        }
    }
    if ( dir == 360 )
    {
        dir = 0;
    }

// !!!! ???? TODO : compare with "0+-epsilon" ???? !!!!
// and set speed to 0.0
    if ( speed == 0 )
    {
        dir = targetDir;
    }
//}
//////// DIR - END

if (speedX < 0)
    sign_speedx = 1;
if (speedY < 0)
    sign_speedy = 1;
if (speed < 0)
    sign_speed = 1;

// update  position
posX += speedX;
posY += speedY;


if ( !bPush )
{

    // update "targetDist" or "dir"
    if ( targetDir == dir )
    {
        if ( (targetDist != INF) && (targetDist > 0) )
        {
            targetDist -= speed;

// !!!! ???? TODO : OK ? ???? !!!!
            // if reached target or went too far => stop
            if ( targetDist <= 0.0 )
            {
                bNewMove = false; // should be useless

                targetDist = 0.0;

/*
                bAccel = false;
                bFri = false;

                speed = 0.0;
                speedX = 0.0;
                speedY = 0.0;
*/

// !!!! ???? TODO : other variables to set ? ???? !!!!
            }

        }

    }
//////// DIR - BEGIN
/*
    else
    {

        if ( speedX == 0 )
        {
            if ( speedY == 0 )
            {
                dir = 0;
            }
            else if ( speedY > 0 )
            {
                dir = 90;
            }
            else
            {
                dir = 270;
            }
        }
        else
        {
// !!!! TODO : find a way to avoid using "atan" !!!!
            dir = arctan( speedY / speedX );
            dir = round (dir * ( 180 / 3.141592 ));

            if ( speedX > 0 )
            {
                if ( speedY < 0 )
                {
                    dir = 360 + dir;
                }
            }
            else
            {
                dir = 180 + dir;
            }
        }

    }
*/
//////// DIR - MID

}
else
{
    bPush = false; // "push" is only needed for one step
}


////
// "invert y" hack (for gamemaker)
x = posX;
y = game_height - posY;
////

