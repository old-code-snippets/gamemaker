
/*
!!!!

TODO :
------

1. modify code to handle relative target movement instead of absolute

=> 2 functions : relativeTargetMove(dx,dy) & absoluteTargetMove(x,y)

relativeTargetMove(dx,dy) : set deltaX & deltaY (instead of targetX and targetY, that become useless)

absoluteTargetMove(x,y) : relativeTargetMove(posX - x, posY - y)

=> in update function :
    posX += speedX;
    posY += speedY;
    deltaX -= speedX;
    deltaY -= speedY;

----

2. where to use integers or float (or double) ?

  - could use "big ints" instead of floats,
  => instead of doing
      f = (int)(f*100)/100
  could do
      i = (int)(f*100)
  (and work with values 100 times bigger ...)

----

3. should avoid uses of atan, cos, sin, ...

----

4. handle weight in movements
	~pos += speed / weight;
 (somethnig like that, to make movements slower when heavier)


!!!!


// "global" (object level)

int dir;
//?int posX;
float posX;
//?int posY;
float posY;

//! if using the radians values, need to use floats instead of integers !
bool bTarget;
int targetDir;

//?int targetDist;
float targetDist;
//?int targetX;
float targetX;
//?int targetY;
float targetY;

bool bAccel;
bool bFri;


////

// "local" ("update" function level)

bool bNewMove;

float accelX;
float accelY;
float friX;
float friY;

//?int deltaX;
double deltaX;
//?int deltaY;
double deltaY;




*/


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// !!!! TODO : move to another file ... !!!!
//////// FUNCTIONS - MID
/*
//float computeLength( int deltaX, int deltaY )
float computeLength( float deltaX, float deltaY )
{
    float length;

    if ( deltaX == 0 )
    {
        if ( deltaY == 0 )
        {
            length = 0;
        }
        else if ( deltaY > 0 )
        {
            length = deltaY;
        }
        else // deltaY < 0
        {
            length = -deltaY;
        }
    }
    else // deltaX != 0
    {
        if ( deltaY == 0 )
        {
            if ( deltaX > 0 )
            {
                length = deltaX;
            }
            else // deltaX < 0
            {
                length = -deltaX;
            }
        }
        else // deltaY != 0
        {
            length = sqrt( (deltaX * deltaX) + (deltaY * deltaY) );
// !!!! ???? TODO : OK (round to 2 decimals) ? ???? !!!!
// => or should use bigger values ? 1 decimal ?
            length = ( (int)( length * 100 ) ) / 100.0;
        }
    }
    return length;
}

//int computeAngle( int deltaX, int deltaY )
int computeAngle( float deltaX, float deltaY )
{
    int angle;

    if ( deltaX == 0 )
    {
        if ( deltaY == 0 )
        {
            angle = -1; // no angle!
        }
        else if ( deltaY > 0 )
        {
            angle = 90;
        }
        else // deltaY < 0
        {
            angle = 270;
        }
    }
    else // deltaX != 0
    {
        if ( deltaX > 0 )
        {
            if ( deltaY == 0 )
            {
                angle = 0;
            }
            else // deltaY != 0
            {
// !!!! TODO : find a way to avoid using "atan" !!!!
// !!!! ???? TODO : maybe no need to convert directions from radians to degrees ? ???? !!!!
// => can maybe do everything in radians ?
//                angle = (int) ( Math.toDegrees( Math.atan2( deltaY / deltaX ) ) );
                angle = arctan( deltaY / deltaX );
                angle = round (angle * ( 180 / 3.141592 ));

                if ( deltaY < 0 )
                {
                    angle = 360 + angle;
                }
            }
        }
        else // deltaX < 0
        {
            if ( deltaY == 0 )
            {
                angle = 180;
            }
            else // deltaY != 0
            {
// !!!! TODO : find a way to avoid using "atan" !!!!
// !!!! ???? TODO : maybe no need to convert directions from radians to degrees ? ???? !!!!
// => can maybe do everything in radians ?
//                angle = (int) ( Math.toDegrees( Math.atan2( deltaY / deltaX ) ) );
                angle = arctan( deltaY / deltaX );
                angle = round (angle * ( 180 / 3.141592 ));

                angle = 180 + angle;
            }
        }

        if ( angle == 360 )
        {
            angle = 0;
        }
    }
    return angle;
}
*/
//////// FUNCTIONS - END

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


// update (...)
//{

    // new move
    if ( bNewMove )
    {
// !!!! ???? TODO : needed ? ???? !!!!
// => or could be done later, if targetDist > 0 ...
        bAccel = true;
//?        bFri = false;

        if ( !bTarget )
        {
            bNewMove = false;

// !!!! ???? TODO : something else to do ? ???? !!!!
// ...
        }
        else
        {
            // determine direction and distance to target
            deltaX = targetX - posX;
            deltaY = targetY - posY;

////
// !!!! TODO : to remove (if using integers) !!!!
// => to "round" the value to 2 decimals
            deltaX = ( (int)( deltaX * 100 ) ) / 100.0;
            deltaY = ( (int)( deltaY * 100 ) ) / 100.0;
////

//////// FUNCTIONS - BEGIN
            if ( deltaX == 0 )
            {
                if ( deltaY == 0 )
                {
                    targetDist = 0;
                }
                else if ( deltaY > 0 )
                {
                    targetDist = deltaY;
                }
                else // deltaY < 0
                {
                    targetDist = -deltaY;
                }
            }
            else // deltaX != 0
            {
                if ( deltaY == 0 )
                {
                    if ( deltaX > 0 )
                    {
                        targetDist = deltaX;
                    }
                    else // deltaX < 0
                    {
                        targetDist = -deltaX;
                    }
                }
                else // deltaY != 0
                {
                    targetDist = sqrt( (deltaX * deltaX) + (deltaY * deltaY) );
                    targetDist = ( (int)( targetDist * 100 ) ) / 100.0;
                }
            }
//////// FUNCTIONS - MID
//            targetDist = computeLength( deltaX, deltaY );
//////// FUNCTIONS - END

// !!!! ???? TODO : should maybe compare with an epsilon ? ???? !!!!
// ( not exactly 0.0 )
            if ( targetDist == 0.0 )
            {
                bTarget = false;
                targetDir = dir;
            }
            else
            {

//////// FUNCTIONS - BEGIN
                if ( deltaX == 0 )
                {
                    if ( deltaY == 0 )
                    {
                        targetDir = -1; // no angle!
                    }
                    else if ( deltaY > 0 )
                    {
                        targetDir = 90;
                    }
                    else // deltaY < 0
                    {
                        targetDir = 270;
                    }
                }
                else // deltaX != 0
                {
                    if ( deltaX > 0 )
                    {
                        if ( deltaY == 0 )
                        {
                            targetDir = 0;
                        }
                        else // deltaY != 0
                        {
                            targetDir = arctan( deltaY / deltaX );
                            targetDir = round (targetDir * ( 180 / 3.141592 ));

                            if ( deltaY < 0 )
                            {
                                targetDir = 360 + angle;
                            }
                        }
                    }
                    else // deltaX < 0
                    {
                        if ( deltaY == 0 )
                        {
                            targetDir = 180;
                        }
                        else // deltaY != 0
                        {
                            targetDir = arctan( deltaY / deltaX );
                            targetDir = round (targetDir * ( 180 / 3.141592 ));

                            targetDir = 180 + angle;
                        }
                    }

                    if ( targetDir == 360 )
                    {
                        targetDir = 0;
                    }
                }
//////// FUNCTIONS - MID
//                targetDir = computeAngle( deltaX, deltaY );
//////// FUNCTIONS - END
                if ( targetDir == -1 )
                {
                    targetDir = dir;
                }

            }
        }


        if ( targetDir == dir )
        {
            bNewMove = false;

            if ( (targetDist > 0.0) && (targetDist < da + df) )
            {
// !!!! TODO : should make a separate function (for code clarity) !!!!
// !!!! TODO : check formula is ok !!!!
                stopSpeed = sqrt( ( FRI / (FRI + ACC) ) * ( (speed * speed) + ACC * ( (2 * targetDist) + speed ) ) );
// !!!! ???? TODO : OK to "round" to 2 decimals ? ???? !!!!
                stopSpeed = ( (int)( stopSpeed * 100 ) ) / 100.0;

                stopSpeed = ( (stopSpeed - speed) / ACC ) + 0.5;
                stopSpeed = floor( stopSpeed );
                stopSpeed *= ACC;
                stopSpeed += speed;
// !!!! ???? TODO : need to make sure "stopSpeed" is smaller than "maxSpeed" ? ???? !!!!
// => if not, can go faster than maxSpeed => what we want in "moveForward" and "move" cases !

// !!!! TODO : should make a separate function (for code clarity) !!!!
// !!!! TODO : check that stopDist is correctly initialised if not passing here !!!!
// !!!! TODO : check formula is ok, and number of decimals !!!!
// => should probably "round" to 2 decimals ...
                stopDist = ( 1 / (2 * FRI) ) * stopSpeed;
                stopDist -= 0.5 * stopSpeed;
            }
        }

    }


    // if new target direction => determine new acceleration
    if ( oldTargetDir != targetDir )
    {
        oldTargetDir = targetDir;


// !!!! ???? TODO : multiply by ACC or FRI here, or when computing speed ? ???? !!!!
// => if here, less multiplications, but more variables
// (and useless computing of fri or acc if in other case)
/*
// !!!! ???? TODO : better to compute with this instead of cos/sin ? ???? !!!!
// (if not, useless ...)
        if ( bTarget )
        {
            accelX = ( deltaX * ACC ) / targetDist;
            accelY = ( deltaY * ACC ) / targetDist;
        }
        else
        {
*/
        tmpCos = cos ( targetDir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : OK with 2 decimals ? ???? !!!!
        tmpCos = ( (int)( tmpCos * 100 ) ) / 100.0;

        tmpSin = sin ( targetDir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : OK with 2 decimals ? ???? !!!!
        tmpSin = ( (int)( tmpSin * 100 ) ) / 100.0;

        accelX = tmpCos * ACC;
        accelY = tmpSin * ACC;

//        }

// !!!! ???? TODO : OK with 2 decimals ? ???? !!!!
        accelX = ( (int)( accelX * 100 ) ) / 100.0;
        accelY = ( (int)( accelY * 100 ) ) / 100.0;

    }

    // if new direction => determine new friction
    if ( oldDir != dir )
    {
        oldDir = dir;

        if ( dir == targetDir )
        {
// !!!! ???? TODO : OK to use *(FRI/ACC) ? ???? !!!!
// => to avoid recomputing cos and sin ...
            friX = ( accelX / ACC ) * FRI;
            friY = ( accelY / ACC ) * FRI;
        }
        else
        {

            tmpCos = cos ( dir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : OK with 2 decimals ? ???? !!!!
            tmpCos = ( (int)( tmpCos * 100 ) ) / 100.0;

            tmpSin = sin ( dir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : OK with 2 decimals ? ???? !!!!
            tmpSin = ( (int)( tmpSin * 100 ) ) / 100.0;

            friX = tmpCos * FRI;
            friY = tmpSin * FRI;
        }

// !!!! ???? TODO : OK with 2 decimals ? ???? !!!!
        friX = ( (int)( accelX * 100 ) ) / 100.0;
        friY = ( (int)( accelY * 100 ) ) / 100.0;

    }



    // "push" boolean to precise we're doing a "move"
    if ( !bPush )
    {

        // determine if need to accelerate or slow down
        if ( (targetDist <= 0.0) || (stopDist == INF) )
        {
            // reached target or need to stop asap
            // => slow down
            bAccel = false;
            bFri = true;

        }
        else if ( targetDist <= stopDist )
        {
            // getting close to target
            // => slow down (except if speed = 0)
// !!!! ???? TODO : OK ? ???? !!!!
            if ( speed > 0.0 )
            {
                bAccel = false;
                bFri = true;

                // directions are "very different"
                tmpAngle = abs( dir - targetDir );
                if ( (tmpAngle > 135) && (tmpAngle < 225) )
                {
                    // accelerate in "different" direction
                    bAccel = true;
                }

            }
            else
            {
                // avoid stopping before reached target
                bAccel = true;
                bFri = false;
            }

        }
        else if ( targetDist != INF )
        {
            tmpAngle = abs( dir - targetDir );

// !!!! ???? TODO : OK to consider this angle ? ???? !!!!
            if ( (speed < stopSpeed) || ( (tmpAngle > 45) && (tmpAngle < 315) ) )
            {
                bAccel = true;
            }
            else
            {
                bAccel = false;
            }

            // directions are "not close" => speed up "allignment"
            if ( (tmpAngle > 90) && (tmpAngle < 270) )
            {
                bFri = true;
            }
            else
            {
                bFri = false;
            }

        }
        else // targetDist == INF
        {
// !!!! TODO : could be avoided if set at beginning of move !!!!
// => but wouldn't change much ...
            bAccel = true;
            bFri = false;
        }



        // acceleration
        if ( bAccel )
        {

            speedX += accelX;
            speedY += accelY;

//////// FUNCTIONS - BEGIN
            speed = sqrt ( (speedX * speedX) + (speedY * speedY) );
// !!!! ???? TODO : OK (round to 2 decimals) ? ???? !!!!
// => or should use bigger values ? 1 decimal ?
            speed = ( (int)(speed * 100 ) ) / 100.0;
//////// FUNCTIONS - MID
//            speed = computeLength( speedX, speedY );
//////// FUNCTIONS - END

// !!!! ???? TODO : maybe shouldn't test here, but after friction ? ???? !!!!
// => because when have accel AND fri, spee will be reduced by fri !
// !!!! ???? TODO : compare with "stopSpeed+-epsilon" ? ???? !!!!
            if ( speed == stopSpeed )
            {
                bAccel = false;
//...
            }
            else if ( speed > stopSpeed )
            {
                bAccel = false;
//...

// !!!! ???? TODO : what to do ? ???? !!!!
// => if set speed to stopSpeed, there's a problem when "push" moves ...
/*
                if ( stopDist != INF )
                {
                    speedX *= stopSpeed / speed;
                    speedY *= stopSpeed / speed;
                    speed = stopSpeed;
                }
*/
                bFri = true;

            }

        }

        if ( speed == 0 )
        {
            bFri = false;
        }

        // friction
        if ( bFri )
        {

            // determine speedX
            if ( sign( speedX - friX) != sign( speedX ) )
            {
                speedX = 0.0;
            }
            else
            {
                speedX -= friX;
            }

            // determine speedY
            if ( sign( speedY - friY) != sign( speedY ) )
            {
                speedY = 0.0;
            }
            else
            {
                speedY -= friY;
            }


            // determine speed
//////// FUNCTIONS - BEGIN
            if ( speedX == 0 )
            {
                if ( speedY == 0 )
                {
                    speed = 0.0;
                }
                else if ( speedY > 0 )
                {
                    speed = speedY;
                }
                else // speedY < 0
                {
                    speed = -speedY;
                }
            }
            else // speedX != 0
            {
                if ( speedY == 0 )
                {
                    if ( speedX > 0 )
                    {
                        speed = speedX;
                    }
                    else // speedX < 0
                    {
                        speed = -speedX;
                    }
                }
                else // speedY != 0
                {
                    speed = sqrt ( (speedX * speedX) + (speedY * speedY) );
// !!!! ???? TODO : OK (round to 2 decimals) ? ???? !!!!
// => or should use bigger values ? 1 decimal ?
                    speed = ( (int)( speed * 100 ) ) / 100.0;
                }
            }
//////// FUNCTIONS - MID
//            speed = computeLength( speedX, speedY );
//////// FUNCTIONS - END


            if ( speed == 0.0 )
            {
                bFri = false;
//...
            }

        }

    }
    else // bPush
    {
        // new speed => probably new dir => new move
        bNewMove = true;

//////// FUNCTIONS - BEGIN
        speed = sqrt( (speedX * speedX) + (speedY * speedY) );
// !!!! ???? TODO : OK (round to 2 decimals) ? ???? !!!!
// => or should use bigger values ? 1 decimal ?
        speed = ( (int)( speed * 100 ) ) / 100.0;
//////// FUNCTIONS - MID
//        speed = computeLength( speedX, speedY );
//////// FUNCTIONS - END
    }




//////// DIR - MID
// !!!! TODO : should avoid recomputing "dir" every time ... !!!!
// => need to determine when "dir" has changed ...
//  => ???
//{

//////// FUNCTIONS - BEGIN
    if ( speedX == 0 )
    {
        if ( speedY == 0 )
        {
            dir = -1;
        }
        else if ( speedY > 0 )
        {
            dir = 90;
        }
        else
        {
            dir = 270;
        }
    }
    else
    {
        if ( speedX > 0 )
        {
            if ( speedY == 0 )
            {
                dir = 0;
            }
            else // speedY != 0
            {
                dir = arctan( speedY / speedX );
                dir = round (dir * ( 180 / 3.141592 ));

                if ( speedY < 0 )
                {
                    dir = 360 + dir;
                }
            }
        }
        else // speedX < 0
        {
            if ( speedY == 0 )
            {
                dir = 180;
            }
            else // speedY != 0
            {
                dir = arctan( speedY / speedX );
                dir = round (dir * ( 180 / 3.141592 ));

                dir = 180 + dir;
            }
        }

        if ( dir == 360 )
        {
            dir = 0;
        }
    }
//////// FUNCTIONS - MID
//    dir = computeAngle( speedX, speedY );
//////// FUNCTIONS - END

    if ( dir == -1 )
    {
        dir = targetDir;
    }

//}
//////// DIR - END




    // update  position
    posX += speedX;
    posY += speedY;


    if ( !bPush )
    {

        // update "targetDist"
        if ( targetDir == dir )
        {
            if ( (targetDist != INF) && (targetDist > 0) )
            {
                targetDist -= speed;

// !!!! ???? TODO : OK ? ???? !!!!
                // if reached target or went too far => stop
                if ( targetDist <= 0.0 )
                {
                    bTarget = false;
                    targetDist = 0.0;
                }

            }

        }

    }
    else
    {
        bPush = false; // "push" is only needed for one step
    }


////
// "invert y" hack (for gamemaker)
x = posX;
y = game_height - posY;
////

