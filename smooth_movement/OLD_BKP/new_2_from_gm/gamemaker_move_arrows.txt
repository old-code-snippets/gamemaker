
=> move

move( iDir, (iSpeed) )
{
    bPush = true;


        tmpCos = cos ( iDir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : what values to use ? ???? !!!!
        if ( abs( tmpCos ) < 0.01 )
        {
            tmpCos = 0.0;
        }
        else if ( tmpCos > 0.99 )
        {
            tmpCos = 1.0;
        }
        else if ( tmpCos < -0.99 )
        {
            tmpCos = -1.0;
        }

        tmpSin = sin ( iDir * ( 3.141592 / 180 ) );
// !!!! ???? TODO : what values to use ? ???? !!!!
        if ( abs( tmpSin ) < 0.01 )
        {
            tmpSin = 0;
        }
        else if ( tmpSin > 0.99 )
        {
            tmpSin = 1.0;
        }
        else if ( tmpSin < -0.99 )
        {
            tmpSin = -1.0;
        }

    speedX += tmpCos * iSpeed;
    speedY += tmpSin * iSpeed;

}

----------------------------------------------------------------

UP
--

bPush = true;
speedY += iSpeed;

----------------------------------------------------------------

DOWN
----

bPush = true;
speedY -= iSpeed;

----------------------------------------------------------------

RIGHT
-----

bPush = true;
speedX += iSpeed;

----------------------------------------------------------------

LEFT
----

bPush = true;
speedX -= iSpeed;

----------------------------------------------------------------
