
=> moveStart

moveStart ( iDir, (iSpeed) )
{
    bNewMove = true;

    bTarget = false;

    targetDir = iDir;
    targetDist = INF;

    stopDist = 0;
//    stopSpeed = maxSpeed;
    stopSpeed = iSpeed;
}

----------------------------------------------------------------

+ (?)
-----
// start moving to iSpeed, in direction pointed by mouse

bNewMove = true;
bTarget = false;

tmpDeltaX = mouse_x - posX;
tmpDeltaY = (game_height - mouse_y) - posY;
if ( tmpDeltaX == 0 )
{
    if ( tmpDeltaY == 0 )
    {
        targetDir = dir;
    }
    else if ( tmpDeltaY > 0 )
    {
        targetDir = 90;
    }
    else
    {
        targetDir = 270;
    }
}
else
{
    targetDir = arctan( tmpDeltaY / tmpDeltaX );
    targetDir = round( targetDir * ( 180 / 3.141592 ));

    if ( tmpDeltaX > 0 )
    {
        if ( tmpDeltaY < 0 )
        {
            targetDir = 360 + targetDir;
        }
    }
    else
    {
        targetDir = 180 + targetDir;
    }
}

targetDist = INF;

stopDist = 0;
stopSpeed = 10.0;

================================================================

=> moveStop

moveStop ()
{
// useless ?
//?    bNewMove = true;

    bTarget = false;

    targetDir = dir;
    targetDist = 0;

    stopDist = INF;
// useless ?
//?    stopSpeed = 0.0;
}

----------------------------------------------------------------

//bNewMove = true;

bTarget = false;

targetDir = dir;
targetDist = 0.0;

stopDist = INF;
//stopSpeed = 0.0;

----------------------------------------------------------------
