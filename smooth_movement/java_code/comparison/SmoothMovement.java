
/*
!!!!

TODO :
------

1. where to use integers or float (or double) ?

  - could use "big ints" instead of floats,
  => instead of doing
      f = (int)(f*100)/100
  could do
      i = (int)(f*100)
  (and work with values 100 times bigger ...)

?=> use integers with big values : multiply every value by 100 (to act as if were keeping 2 decimals)

OR:
=> Store position as an integer, but keep a �remainder� stored in a float. When integrating position,
 compute the delta-movement as a float, add the remainder to the delta-movement,
 then add the integer part of this value to the position, and the fractional part to the �remainder� field.
 On the next frame, the remainder will get added back in. The advantage of this method is that you�re using
 an integer everywhere except for movement, ensuring that you won�t have floating point complications elsewhere,
 and increasing performance. This technique is also very suitable if you have some framework in which the
 position of the object has to be an integer, or where it is a float, but that same position is used directly
 by the rendering system � in that case, you can use the framework-provided float position to store integer
 values only, to make sure that the rendering is always aligned to pixels.



----

2. handle weight in movements
	~pos += speed / weight;
 (somethnig like that, to make movements slower when heavier)


*/

/*
TODO :
- reduce number of roundings ( heavy ! )
  => OR: change every variables to INTs and multiply all values by 100 ?
  ( ! but, careful about precision ! )

- see how to handle "ACC" & "FRI"
  => better as constants ? ( could be variable, if want ie to run ... )


- check if Y axis not inverted

- "time_elapsed" doesn't work ...
  => need to do as in Replica Island, with a "target velocity"

*/

public class SmoothMovement
{
// !!!! TODO : move to a more global class !!!!
// => related to object
// "global" (object level)

// !!!! ???? TODO : what value ? ???? !!!!
    private static final float INF = 10000.0f;

// !!!! ???? TODO : OK with 2 decimals ? ???? !!!!
    private static final int PRECISION = 100;
    private static final float PI = 3.141592f;
    private static final float DEG_TO_RAD = PI / 180.0f;

// !!!! ???? TODO : should be variables ? ???? !!!!
    private static final float ACC = 0.2f;
    private static final float FRI = 0.5f;
//?    private static final float CONST_FACTOR = FRI / ( FRI + ACC );



    private float mMaxSpeed;
// !!!! TODO : need to be recomputed when change mMaxSpeed !!!!
    private float mDistanceAccel;
    private float mDistanceFri;

// !!!! ???? TODO : constant ? variable depending on speed ? ... ? ???? !!!!
    private float mMinDist;


    private float mPosX;
    private float mPosY;

    private int mDir;
    private float mSpeed;
    private float mSpeedX;
    private float mSpeedY;


    private boolean mNewMove;

    private boolean mPush;
    private boolean mSlowDown;

    private float mStopDist;
    private float mStopSpeed;

    private boolean mTarget;
    private int mTargetDir;
    private float mTargetDist;
    private float mTargetX;
    private float mTargetY;


    private boolean mAccel;
    private float mAccelX;
    private float mAccelY;
    private boolean mFri;
    private float mFriX;
    private float mFriY;


////////
    private static final int INF_int = 100000;
    private static final int ACC_int = 20;
    private static final int FRI_int = 50;

    private int mMaxSpeed_int;
    private int mDistanceAccel_int;
    private int mDistanceFri_int;
    private int mMinDist_int;

    private int mPosX_int;
    private int mPosY_int;

    private int mDir_int;
    private int mSpeed_int;
    private int mSpeedX_int;
    private int mSpeedY_int;

    private boolean mNewMove_int;
    private boolean mPush_int;
    private boolean mSlowDown_int;

    private int mStopDist_int;
    private int mStopSpeed_int;

    private boolean mTarget_int;
    private int mTargetDir_int;
    private int mTargetDist_int;
    private int mTargetX_int;
    private int mTargetY_int;

    private boolean mAccel_int;
    private int mAccelX_int;
    private int mAccelY_int;
    private boolean mFri_int;
    private int mFriX_int;
    private int mFriY_int;
////////


    public SmoothMovement()
    {
        mMaxSpeed = 10.0f;
        mDistanceAccel = mMaxSpeed * ( ( mMaxSpeed / ACC ) + 1.0f ) / 2.0f;
        mDistanceFri = mMaxSpeed * ( ( mMaxSpeed / FRI ) - 1.0f ) / 2.0f;
// !!!! ???? TODO : what value ? ???? !!!!
// ( constant ? variable depending on speed ? ... ? )
        mMinDist = 1.0f;

        mDir = 0;
        mSpeed = 0.0f;
        mSpeedX = 0.0f;
        mSpeedY = 0.0f;


        mNewMove = false;
        mPush = false;
        mSlowDown = false;

// !!!! ???? TODO : OK ? ???? !!!!
        mStopDist = 0.0f;
        mStopSpeed = 0.0f;

        mTarget = false;
        mTargetDist = 0.0f;
        mTargetDir = mDir;
        mTargetX = 0.0f;
        mTargetY = 0.0f;

        mAccel = false;
        mAccelX = ACC;
        mAccelY = 0.0f;
        mFri = false;
        mFriX = FRI;
        mFriY = 0.0f;

////////
        mMaxSpeed_int = 1000;
        mDistanceAccel_int = mMaxSpeed_int * ( ( mMaxSpeed_int / ACC_int ) + 1 ) / 2;
        mDistanceFri_int = mMaxSpeed_int * ( ( mMaxSpeed_int / FRI_int ) - 1 ) / 2;
// !!!! ???? TODO : what value ? ???? !!!!
// ( constant ? variable depending on speed ? ... ? )
        mMinDist_int = 100;

        mDir_int = 0;
        mSpeed_int = 0;
        mSpeedX_int = 0;
        mSpeedY_int = 0;

        mNewMove_int = false;
        mPush_int = false;
        mSlowDown_int = false;

// !!!! ???? TODO : OK ? ???? !!!!
        mStopDist_int = 0;
        mStopSpeed_int = 0;

        mTarget_int = false;
        mTargetDist_int = 0;
        mTargetDir_int = mDir_int;
        mTargetX_int = 0;
        mTargetY_int = 0;

        mAccel_int = false;
        mAccelX_int = ACC_int;
        mAccelY_int = 0;
        mFri_int = false;
        mFriX_int = FRI_int;
        mFriY_int = 0;
////////
    }


// !!!! TODO : don't have a "time_elpased" !!!!
// => so only to be used with constant frame rate !!!!
//    ( need to call the function at constant interval ) 
    public void update()
    {
        // new move
        if ( mNewMove )
        {
            // determine target direction & distance, stop speed & stop distance
            setTarget();
        }

        // "push" boolean to precise we're doing a "move"
        if ( !mPush )
        {
            // determine if need to accelerate or slow down
            setAction();

            // set the speed according to acceleration and friction
            // accelerate
            if ( mAccel )
            {
// !!!! TODO : should compute acceleration here !!!!

                setAcceleration();
            }

            // slow down
            if ( mFri )
            {
// !!!! TODO : should compute friction here !!!!

                setFriction();
            }
        }
        else // mPush == true
        {
            mPush = false; // "push" is only needed for one step

            // new speed => probably new dir => new move
            mNewMove = true;

            mSpeed = Utils.computeLength( mSpeedX, mSpeedY );
        }

// !!!! TODO : should be the opposite !!!!
// => compute "speedX" and "speedY" from "speed" and "dir" ( dir should have been computed after collisions )
// ? or maybe already have "speedX" and "speedY" from collision handling ?

// !!!! TODO : should avoid recomputing "dir" every time ... !!!!
// => need to determine when "dir" has changed ...
//  => ???
//        {

        mDir = Utils.computeAngle( mSpeedX, mSpeedY );
        if ( mDir == -1 )
        {
            mDir = mTargetDir;
        }
        if ( ( mTargetDir < mDir + 5 ) && ( mTargetDir > mDir - 5 ) )
        {
            mDir = mTargetDir;
        }
//        }



// !!!! TODO : position shouldn't be computed like this !!!!
// => should "receive" new position from the collision handler
// ( => and from that we can also get the new "deltaX" and "deltaY" )
// ? better/easier to get the new position or the new offset from previous position ?
        // update position
        mPosX += mSpeedX;
        mPosY += mSpeedY;

// !!!! TODO : can't do that here as the speed will not reflect the whole movement (if includes bounces, etc.) !!!!
// => need to recompute deltaX & deltaY from position and target position
// ( must do it at beginning of this update function, as need info such as "bTarget", etc. )
// OR
// !!!! Should be computed when doing the movement !!!!
// => when exiting the collisions handling function, we have a "new dir", "new speed", and "new pos" (or "new offset")
//    => so can determine the "new pos" and "new deltas" from it
        // update target position
        if ( mTarget )
        {
            mTargetX -= mSpeedX;
            mTargetY -= mSpeedY;
        }


// !!!! TODO : test without this !!!!
// => see if really needed ( and if removing it doesn't impact performances too much )
// ( to check if can remove everything that's after setting the new position )
/*
        if ( !mPush )
        {
            // update "mTargetDist"
            if ( mTargetDir == mDir )
            {
                if ( ( mTargetDist != INF ) && ( mTargetDist > 0.0f ) )
                {
                    mTargetDist -= mSpeed;

// !!!! ???? TODO : OK ? ???? !!!!
                    // if reached target or went too far => stop
                    if ( mTargetDist <= 0.0f )
                    {
                        mTarget = false;
                        mTargetDist = 0.0f;
                    }
                }
            }
        }
        else
        {
            mPush = false; // "push" is only needed for one step
        }
*/
    }
////////
    public void update_int()
    {
        if ( mNewMove_int )
        {
            // determine target direction & distance, stop speed & stop distance
            setTarget_int();
        }

        if ( !mPush_int )
        {
            setAction_int();

            if ( mAccel_int )
            {
                setAcceleration_int();
            }
            if ( mFri_int )
            {
                setFriction_int();
            }
        }
        else // mPush_int == true
        {
            mPush_int = false; // "push" is only needed for one step
            mNewMove_int = true;

            mSpeed_int = Utils.computeLength_int( mSpeedX_int, mSpeedY_int );
        }

// !!!! TODO : should be the opposite !!!!
// => compute "speedX" and "speedY" from "speed" and "dir" ( dir should have been computed after collisions )
// ? or maybe already have "speedX" and "speedY" from collision handling ?

// !!!! TODO : should avoid recomputing "dir" every time ... !!!!
// => need to determine when "dir" has changed ...
//  => ???
//        {

        mDir_int = Utils.computeAngle_int( mSpeedX_int, mSpeedY_int );
        if ( mDir_int == -1 )
        {
            mDir_int = mTargetDir_int;
        }
        if ( ( mTargetDir_int < mDir_int + 5 ) && ( mTargetDir_int > mDir_int - 5 ) )
        {
            mDir_int = mTargetDir_int;
        }
//        }



// !!!! TODO : position shouldn't be computed like this !!!!
// => should "receive" new position from the collision handler
// ( => and from that we can also get the new "deltaX" and "deltaY" )
// ? better/easier to get the new position or the new offset from previous position ?
        // update position
        mPosX_int += mSpeedX_int;
        mPosY_int += mSpeedY_int;

// !!!! TODO : can't do that here as the speed will not reflect the whole movement (if includes bounces, etc.) !!!!
// => need to recompute deltaX & deltaY from position and target position
// ( must do it at beginning of this update function, as need info such as "bTarget", etc. )
// OR
// !!!! Should be computed when doing the movement !!!!
// => when exiting the collisions handling function, we have a "new dir", "new speed", and "new pos" (or "new offset")
//    => so can determine the "new pos" and "new deltas" from it
        // update target position
        if ( mTarget_int )
        {
            mTargetX_int -= mSpeedX_int;
            mTargetY_int -= mSpeedY_int;
        }


// !!!! TODO : test without this !!!!
// => see if really needed ( and if removing it doesn't impact performances too much )
// ( to check if can remove everything that's after setting the new position )
/*
        if ( !mPush )
        {
            // update "mTargetDist"
            if ( mTargetDir == mDir )
            {
                if ( ( mTargetDist != INF ) && ( mTargetDist > 0.0f ) )
                {
                    mTargetDist -= mSpeed;

// !!!! ???? TODO : OK ? ???? !!!!
                    // if reached target or went too far => stop
                    if ( mTargetDist <= 0.0f )
                    {
                        mTarget = false;
                        mTargetDist = 0.0f;
                    }
                }
            }
        }
        else
        {
            mPush = false; // "push" is only needed for one step
        }
*/
    }
////////

    private void setTarget()
    {
        if ( !mTarget )
        {
            mNewMove = false;

// !!!! ???? TODO : something else to do ? ???? !!!!
// ...
        }
        else
        {
            // determine direction and distance to target
// !!!! ???? TODO : compute targetDir first ? ???? !!!!
            mTargetDist = Utils.computeLength( mTargetX, mTargetY );

// !!!! TODO : should compare with an epsilon !!!!
// ( not exactly 0.0 )
            if ( targetDist == 0.0f )
            {
                mTarget = false;
                mTargetDir = mDir;
            }
            else
            {
                mTargetDir = Utils.computeAngle( mTargetX, mTargetY );
                if ( mTargetDir == -1 )
                {
//// NEW - MID
// !!!! ???? TODO : shoud add this if reverse order of targetDist & targetDir computation ? ???? !!!!
//?                        mTarget = false;
// => and also add "mTargetDist = 0.0f;"
//// NEW - END
                    mTargetDir = mDir;
                }
            }
        }

// !!!! TODO : find best value ... !!!!
        if ( ( mTargetDir < mDir + 5 ) && ( mTargetDir > mDir - 5 ) )
        {
            mDir = mTargetDir;
            mNewMove = false;

            if ( mSlowDown && ( mTargetDist > 0.0f )
                 && ( mTargetDist < mDistanceAccel + mDistanceFri ) )
            {
                computeStopSpeed();
// !!!! TODO : check that mStopDist is correctly initialised if not passing here !!!!
                computeStopDist();

            }
        }
    }
////////
    private void setTarget_int()
    {
        if ( !mTarget_int )
        {
            mNewMove_int = false;

// !!!! ???? TODO : something else to do ? ???? !!!!
// ...
        }
        else
        {
            // determine direction and distance to target
// !!!! ???? TODO : compute targetDir first ? ???? !!!!
            mTargetDist_int = Utils.computeLength_int( mTargetX_int, mTargetY_int );

// !!!! TODO : should compare with an epsilon !!!!
// => not exactly 0 => <100 ?
            if ( targetDist_int == 0 )
            {
                mTarget_int = false;
                mTargetDir_int = mDir_int;
            }
            else
            {
                mTargetDir_int = Utils.computeAngle_int( mTargetX_int, mTargetY_int );
                if ( mTargetDir_int == -1 )
                {
//// NEW - MID
// !!!! ???? TODO : shoud add this if reverse order of targetDist & targetDir computation ? ???? !!!!
//?                        mTarget = false;
// => and also add "mTargetDist_int = 0;"
//// NEW - END
                    mTargetDir_int = mDir_int;
                }
            }
        }

// !!!! TODO : find best value ... !!!!
        if ( ( mTargetDir_int < mDir_int + 5 ) && ( mTargetDir_int > mDir_int - 5 ) )
        {
            mDir_int = mTargetDir_int;
            mNewMove_int = false;

            if ( mSlowDown_int && ( mTargetDist_int > 0 )
                 && ( mTargetDist_int < mDistanceAccel_int + mDistanceFri_int ) )
            {
                computeStopSpeed_int();
// !!!! TODO : check that mStopDist is correctly initialised if not passing here !!!!
                computeStopDist_int();

            }
        }
    }
////////

// !!!! ???? TODO : if needed, could use parameters instead of data members !!!!
// => & set the function as "static" ( & "public ")
//   ( & set constants to the input parameters and return float )
// !!!! TODO : check that the formula is ok !!!!
    private void computeStopSpeed()
    {
// !!!! TODO : could use "CONST_FACTOR" to speed up a bit !!!!
// => but makes it less clear ... ( ! if so need to change the function body ! )
//        mStopSpeed = Math.sqrt( CONST_FACTOR * ( ( mSpeed * mSpeed ) + ACC * ( ( 2.0f * mTargetDist ) + mSpeed ) ) );
// where : CONST_FACTOR = FRI / ( FRI + ACC )

        // mStopSpeed = Math.sqrt( ( FRI / ( FRI + ACC ) ) * ( ( mSpeed * mSpeed ) + ( ACC * ( ( 2 * mTargetDist ) + mSpeed ) ) ) );
        mStopSpeed = ( 2.0f * mTargetDist ) + mSpeed;
        mStopSpeed *= ACC;
        mStopSpeed += ( mSpeed * mSpeed );
        mStopSpeed *= ( FRI / ( FRI + ACC ) );
// !!!! ???? TODO : what is the Math function for "sqrt" ? ???? !!!!
        mStopSpeed = Math.sqrt( mStopSpeed );

        Utils.roundUnsigned( mStopSpeed, PRECISION );

        // get closest value that is multiple of "acceleration"
        mStopSpeed = ( ( mStopSpeed - mSpeed ) / ACC ) + 0.5f;
        mStopSpeed = (int)mStopSpeed;
        mStopSpeed *= ACC;
        mStopSpeed += mSpeed;

// !!!! TODO : shouldn't be necessary to round here !!!!
// => done before, and following operations shouldn't change precision
    }
////////
    private void computeStopSpeed_int()
    {
// !!!! TODO : could use "CONST_FACTOR" to speed up a bit !!!!
// => but makes it less clear ... ( ! if so need to change the function body ! )
//        mStopSpeed = Math.sqrt( CONST_FACTOR * ( ( mSpeed * mSpeed ) + ACC * ( ( 2.0f * mTargetDist ) + mSpeed ) ) );
// where : CONST_FACTOR = FRI / ( FRI + ACC )

        // mStopSpeed = Math.sqrt( ( FRI / ( FRI + ACC ) ) * ( ( mSpeed * mSpeed ) + ( ACC * ( ( 2 * mTargetDist ) + mSpeed ) ) ) );
        mStopSpeed_int = ( 2 * mTargetDist_int ) + mSpeed_int;
        mStopSpeed_int *= ACC_int;
        mStopSpeed_int += ( mSpeed_int * mSpeed_int );
        mStopSpeed_int *= ( FRI_int / ( FRI_int + ACC_int ) );
// !!!! ???? TODO : what is the Math function for "sqrt" ? ???? !!!!
        mStopSpeed_int = Math.sqrt( mStopSpeed_int );

        // get closest value that is multiple of "acceleration"
        mStopSpeed_int = ( ( ( mStopSpeed_int - mSpeed_int ) / ACC_int ) + 0.5f );
        mStopSpeed_int *= ACC_int;
        mStopSpeed_int += mSpeed_int;
    }
////////

// !!!! ???? TODO : if needed, could use parameters instead of data members !!!!
// => & set the function as "static" ( & "public ")
//   ( & set constants to the input parameters and return float )
// !!!! TODO : check that the formula is ok !!!!
    private float computeStopDist()
    {
        // mStopDist = ( ( 1 / ( 2 * FRI ) ) * ( mStopSpeed * mStopSpeed ) ) - ( 1/2 * mStopSpeed );
        mStopDist = ( 1.0f / ( 2.0f * FRI ) ) * mStopSpeed * mSstopSpeed;
        mStopDist -= 0.5f * mStopSpeed;

        Utils.roundUnsigned( mStopDist, PRECISION );
    }
////////
    private float computeStopDist_int()
    {
        // mStopDist = ( ( 1 / ( 2 * FRI ) ) * ( mStopSpeed * mStopSpeed ) ) - ( 1/2 * mStopSpeed );
        // <=> mStopDist = mStopSpeed * ( mStopSpeed - FRI ) / ( 2 * FRI );
        mStopDist_int = (int) ( ( ( mStopSpeed_int - FRI_int ) * mStopSpeed_int ) / ( 2.0f * FRI_int ) );
    }
////////

    private void setAction()
    {
        int tmpAngle;

// !!!! ???? TODO : OK to also check if "close" to target ? ???? !!!!
// ( <= 0.0 || <= mMinDist ) (idem as <= mMinDist)
//        if ( ( mTargetDist <= 0.0f ) || ( mStopDist == INF ) )
        if ( ( mTargetDist <= mMinDist) || ( mStopDist == INF ) )
        {
            // reached target or need to stop asap
            // => slow down
            mAccel = false;
            mFri = true;
        }
        else if ( mTargetDist <= mStopDist )
        {
            // getting close to target
            // => slow down (except if speed = 0)
// !!!! ???? TODO : OK ? ???? !!!!
            if ( mSpeed > 0.0f )
            {
                mAccel = false;
                mFri = true;

                // directions are "very different"
// !!!! ???? TODO : what is the Math function for "abs" ? ???? !!!!
                tmpAngle = Math.abs( mDir - mTargetDir );
// !!!! TODO : find best values for angles !!!!
                if ( ( tmpAngle > 135 ) && ( tmpAngle < 225 ) )
                {
                    // accelerate in "different" direction
                    mAccel = true;
                }
            }
            else
            {
                // avoid stopping before reached target
                mAccel = true;
                mFri = false;
            }
        }
        else if ( mTargetDist != INF )
        {
// !!!! ???? TODO : what is the Math function for "abs" ? ???? !!!!
            tmpAngle = Math.abs( mDir - mTargetDir );

// !!!! TODO : find best values for angles !!!!
            if ( ( mSpeed < mStopSpeed )
                 || ( ( tmpAngle > 45 ) && ( tmpAngle < 315 ) ) )
            {
                mAccel = true;
            }
            else
            {
                mAccel = false;
            }

            // directions are "not close" => speed up "allignment"
// !!!! TODO : find best values for angles !!!!
            if ( ( tmpAngle > 90 ) && ( tmpAngle < 270 ) )
            {
                mFri = true;
            }
            else
            {
                mFri = false;
            }
        }
        else // targetDist == INF
        {
// !!!! TODO : could be avoided if set at beginning of move !!!!
// => but wouldn't change much ...
            mAccel = true;
            mFri = false;
        }
    }
////////
    private void setAction_int()
    {
        int tmpAngle;

// !!!! ???? TODO : OK to also check if "close" to target ? ???? !!!!
// ( <= 0.0 || <= mMinDist ) (idem as <= mMinDist)
//        if ( ( mTargetDist <= 0.0f ) || ( mStopDist == INF ) )
        if ( ( mTargetDist_int <= mMinDist_int ) || ( mStopDist_int == INF_int ) )
        {
            // reached target or need to stop asap
            // => slow down
            mAccel_int = false;
            mFri_int = true;
        }
        else if ( mTargetDist_int <= mStopDist_int )
        {
            // getting close to target
            // => slow down (except if speed = 0)
// !!!! ???? TODO : OK ? ???? !!!!
            if ( mSpeed_int > 0 )
            {
                mAccel_int = false;
                mFri_int = true;

                // directions are "very different"
// !!!! ???? TODO : what is the Math function for "abs" ? ???? !!!!
                tmpAngle = Math.abs( mDir_int - mTargetDir_int );
// !!!! TODO : find best values for angles !!!!
                if ( ( tmpAngle > 135 ) && ( tmpAngle < 225 ) )
                {
                    // accelerate in "different" direction
                    mAccel_int = true;
                }
            }
            else
            {
                // avoid stopping before reached target
                mAccel_int = true;
                mFri_int = false;
            }
        }
        else if ( mTargetDist_int != INF )
        {
// !!!! ???? TODO : what is the Math function for "abs" ? ???? !!!!
            tmpAngle = Math.abs( mDir_int - mTargetDir_int );

// !!!! TODO : find best values for angles !!!!
            if ( ( mSpeed_int < mStopSpeed_int )
                 || ( ( tmpAngle > 45 ) && ( tmpAngle < 315 ) ) )
            {
                mAccel_int = true;
            }
            else
            {
                mAccel_int = false;
            }

            // directions are "not close" => speed up "allignment"
// !!!! TODO : find best values for angles !!!!
            if ( ( tmpAngle > 90 ) && ( tmpAngle < 270 ) )
            {
                mFri_int = true;
            }
            else
            {
                mFri_int = false;
            }
        }
        else // targetDist == INF
        {
// !!!! TODO : could be avoided if set at beginning of move !!!!
// => but wouldn't change much ...
            mAccel_int = true;
            mFri_int = false;
        }
    }
////////

    private void setAcceleration()
    {
// !!!! ???? TODO : static variable in JAVA same as in C ? ???? !!!!
// => if not, define it at class level ...
        static int oldTargetDir = -1;

        // if new target direction => determine new acceleration
        if ( oldTargetDir != mTargetDir )
        {
            oldTargetDir = mTargetDir;

// !!!! ???? TODO : multiply by "ACC" or "FRI" here, or when computing speed ? ???? !!!!
// => if here, less multiplications, but more variables
// (and useless computing of FRI or ACC if in other case)

/*
// !!!! ???? TODO : better to compute with this instead of cos/sin ? ???? !!!!
// (if not, useless ...)
            if ( mTarget )
            {
                mAccelX = ( mTargetX * ACC ) / mTargetDist;
                mAccelY = ( mTargetY * ACC ) / mTargetDist;
            }
            else
            {
*/
            mAccelX = Math.cos ( mTargetDir * DEG_TO_RAD ) * ACC;
            mAccelY = Math.sin ( mTargetDir * DEG_TO_RAD ) * ACC;
//            }

            Utils.roundSigned( mAccelX, PRECISION );
            Utils.roundSigned( mAccelY, PRECISION );
        }

        mSpeedX += mAccelX;
        mSpeedY += mAccelY;

        mSpeed = Utils.computeLength( mSpeedX, mSpeedY );

// !!!! ???? TODO : maybe shouldn't test here, but after friction ? ???? !!!!
// => because when have accel AND fri, speed will be reduced by fri !
// !!!! ???? TODO : compare with "stopSpeed+-epsilon" ? ???? !!!!
        if ( mSpeed == mStopSpeed )
        {
            mAccel = false;
// !!!! ???? TODO : something else to do ? ???? !!!!
        }
        else if ( mSpeed > mStopSpeed )
        {
            mAccel = false;

// !!!! ???? TODO : something else to do ? ???? !!!!

// !!!! ???? TODO : what to do ? ???? !!!!
// => if set speed to stopSpeed, there's a problem when "push" moves ...
/*
            if ( mStopDist != INF )
            {
                mSpeedX *= mStopSpeed / mSpeed;
                mSpeedY *= mStopSpeed / mSpeed;
                mSpeed = mStopSpeed;
            }
*/
            mFri = true;
        }
    }
////////
    private void setAcceleration_int()
    {
// !!!! ???? TODO : static variable in JAVA same as in C ? ???? !!!!
// => if not, define it at class level ...
        static int oldTargetDir_int = -1;

        // if new target direction => determine new acceleration
        if ( oldTargetDir_int != mTargetDir_int )
        {
            oldTargetDir_int = mTargetDir_int;

// !!!! ???? TODO : multiply by "ACC" or "FRI" here, or when computing speed ? ???? !!!!
// => if here, less multiplications, but more variables
// (and useless computing of FRI or ACC if in other case)

/*
// !!!! ???? TODO : better to compute with this instead of cos/sin ? ???? !!!!
// (if not, useless ...)
            if ( mTarget )
            {
                mAccelX = ( mTargetX * ACC ) / mTargetDist;
                mAccelY = ( mTargetY * ACC ) / mTargetDist;
            }
            else
            {
*/
            mAccelX_int = Math.cos ( mTargetDir_int * DEG_TO_RAD ) * ACC_int;
            mAccelY_int = Math.sin ( mTargetDir_int * DEG_TO_RAD ) * ACC_int;
//            }
        }

        mSpeedX_int += mAccelX_int;
        mSpeedY_int += mAccelY_int;

        mSpeed_int = Utils.computeLength_int( mSpeedX_int, mSpeedY_int );

// !!!! ???? TODO : maybe shouldn't test here, but after friction ? ???? !!!!
// => because when have accel AND fri, speed will be reduced by fri !
// !!!! ???? TODO : compare with "stopSpeed+-epsilon" ? ???? !!!!
        if ( mSpeed_int == mStopSpeed_int )
        {
            mAccel_int = false;
// !!!! ???? TODO : something else to do ? ???? !!!!
        }
        else if ( mSpeed_int > mStopSpeed_int )
        {
            mAccel_int = false;

// !!!! ???? TODO : something else to do ? ???? !!!!

// !!!! ???? TODO : what to do ? ???? !!!!
// => if set speed to stopSpeed, there's a problem when "push" moves ...
/*
            if ( mStopDist != INF )
            {
                mSpeedX *= mStopSpeed / mSpeed;
                mSpeedY *= mStopSpeed / mSpeed;
                mSpeed = mStopSpeed;
            }
*/
            mFri_int = true;
        }
    }
////////

    private void setFriction()
    {
        if ( mSpeed == 0.0f )
        {
            mFri = false;
        }
        else
        {
// !!!! ???? TODO : static variable in JAVA same as in C ? ???? !!!!
// => if not, define it at class level ...
            static int oldDir = -1;

            // if new direction => determine new friction
            if ( oldDir != mDir )
            {
                oldDir = mDir;

                if ( mDir == mTargetDir )
                {
// !!!! ???? TODO : OK to use *(FRI/ACC) ? ???? !!!!
// => to avoid recomputing cos and sin ...
                    mFriX = ( mAccelX / ACC ) * FRI;
                    mFriY = ( mAccelY / ACC ) * FRI;
                }
                else
                {
                    mFriX = Math.cos ( mDir * DEG_TO_RAD ) * FRI;
                    mFriY = Math.sin ( mDir * DEG_TO_RAD ) * FRI;
                }

                Utils.roundSigned( mFriX, PRECISION );
                Utils.roundSigned( mFriY, PRECISION );
            }


            // determine speedX
// !!!! ???? TODO : better to use Math function or "home made" function ? ???? !!!!
            if ( Math.sign( mSpeedX - mFriX) != Math.sign( mSpeedX ) )
            {
                mSpeedX = 0.0;
            }
            else
            {
                mSpeedX -= mFriX;
            }

            // determine speedY
// !!!! ???? TODO : better to use Math function or "home made" function ? ???? !!!!
            if ( Math.sign( mSpeedY - mFriY) != Math.sign( mSpeedY ) )
            {
                mSpeedY = 0.0;
            }
            else
            {
                mSpeedY -= mFriY;
            }

            // determine speed
            mSpeed = Utils.computeLength( mSpeedX, mSpeedY );
            if ( mSpeed == 0.0f )
            {
                mFri = false;

// !!!! ???? TODO : something else to do ? ???? !!!!
            }
        }
    }
////////
    private void setFriction_int()
    {
        if ( mSpeed_int == 0 )
        {
            mFri_int = false;
        }
        else
        {
// !!!! ???? TODO : static variable in JAVA same as in C ? ???? !!!!
// => if not, define it at class level ...
            static int oldDir_int = -1;

            // if new direction => determine new friction
            if ( oldDir_int != mDir_int )
            {
                oldDir_int = mDir_int;

                if ( mDir_int == mTargetDir_int )
                {
// !!!! ???? TODO : OK to use *(FRI/ACC) ? ???? !!!!
// => to avoid recomputing cos and sin ...
                    mFriX_int = ( mAccelX_int * FRI_int ) / ACC_int;
                    mFriY_int = ( mAccelY_int * FRI_int ) / ACC_int;
                }
                else
                {
                    mFriX_int = Math.cos ( mDir_int * DEG_TO_RAD ) * FRI_int;
                    mFriY_int = Math.sin ( mDir_int * DEG_TO_RAD ) * FRI_int;
                }
            }


            // determine speedX
// !!!! ???? TODO : better to use Math function or "home made" function ? ???? !!!!
            if ( Math.sign( mSpeedX_int - mFriX_int ) != Math.sign( mSpeedX_int ) )
            {
                mSpeedX_int = 0;
            }
            else
            {
                mSpeedX_int -= mFriX_int;
            }

            // determine speedY
// !!!! ???? TODO : better to use Math function or "home made" function ? ???? !!!!
            if ( Math.sign( mSpeedY_int - mFriY_int ) != Math.sign( mSpeedY_int ) )
            {
                mSpeedY_int = 0;
            }
            else
            {
                mSpeedY_int -= mFriY_int;
            }

            // determine speed
            mSpeed_int = Utils.computeLength_int( mSpeedX_int, mSpeedY_int );
            if ( mSpeed_int == 0 )
            {
                mFri_int = false;

// !!!! ???? TODO : something else to do ? ???? !!!!
            }
        }
    }
////////


    // move along a dir at speed
    public void move( int dir, float speed )
    {
        mPush = true;

// !!!! TO DO : should check that new speed isn't too big !!!!
// => to avoid going too fast if have several "push moves" in a row ...

        mSpeedX += Math.cos ( dir * DEG_TO_RAD ) * speed;
        mSpeedY += Math.sin ( dir * DEG_TO_RAD ) * speed;

        Utils.roundSigned( mSpeedX, PRECISION );
        Utils.roundSigned( mSpeedY, PRECISION );
    }
////////
    public void move_int( int dir, int speed )
    {
        mPush_int = true;

// !!!! TO DO : should check that new speed isn't too big !!!!
// => to avoid going too fast if have several "push moves" in a row ...

        mSpeedX_int += Math.cos ( dir * DEG_TO_RAD ) * speed;
        mSpeedY_int += Math.sin ( dir * DEG_TO_RAD ) * speed;
    }
////////

    // move along the current direction on dist at speed
    public void moveForward( int dist, float speed )
    {
        mNewMove = true;
        mTarget = false;

        mTargetDir = mDir;
        mTargetDist = dist;

        // adapted in update if needed (if dist < da + df)
        mStopDist = mDistanceFri;
        mStopSpeed = speed;
    }
////////
    public void moveForward( int dist, int speed )
    {
        mNewMove_int = true;
        mTarget_int = false;

        mTargetDir_int = mDir_int;
        mTargetDist_int = dist;

        // adapted in update if needed (if dist < da + df)
        mStopDist_int = mDistanceFri_int;
        mStopSpeed_int = speed;
    }
////////

    // start moving along dir up to speed
    public void moveStart( int dir, float speed )
    {
        mNewMove = true;
        mTarget = false;

        mTargetDir = dir;
        mTargetDist = INF;

        mStopDist = 0.0f;
        mStopSpeed = speed;
    }
////////
    public void moveStart( int dir, int speed )
    {
        mNewMove_int = true;
        mTarget_int = false;

        mTargetDir_int = dir;
        mTargetDist_int = INF_int;

        mStopDist_int = 0;
        mStopSpeed_int = speed;
    }
////////

    // start moving along dir up to speed
// !!!! ???? TODO : int or float ? ???? !!!!
//?    public void moveStart( int dir, int posX, int posY )
    public void moveStart( float posX, float posY, float speed )
    {
        mNewMove = true;
        mTarget = false;

        float tmpDeltaX = posX - mPosX;
        float tmpDeltaY = posY - mPosY;

        mTargetDir = Utils.computeAngle( tmpDeltaX, tmpDeltaY );
        if ( mTargetDir == -1 )
        {
            mTargetDir = mDir;
        }

        mTargetDist = INF;

        mStopDist = 0.0f;
        mStopSpeed = speed;
    }
////////
    public void moveStart_int( int posX, int posY, int speed )
    {
        mNewMove_int = true;
        mTarget_int = false;

        int tmpDeltaX = posX - mPosX_int;
        int tmpDeltaY = posY - mPosY_int;

        mTargetDir_int = Utils.computeAngle_int( tmpDeltaX, tmpDeltaY );
        if ( mTargetDir_int == -1 )
        {
            mTargetDir_int = mDir_int;
        }

        mTargetDist_int = INF_int;

        mStopDist_int = 0;
        mStopSpeed_int = speed;
    }
////////

    // stop accelerating
    public void moveStop()
    {
// useless ?
//?    mNewMove = true;
        mTarget = false;

        mTargetDir = mDir;
        mTargetDist = 0.0f;

        mStopDist = INF;
// useless ?
//?        mStopSpeed = 0.0f;
    }
////////
    public void moveStop_int()
    {
// useless ?
//?    mNewMove_int = true;
        mTarget_int = false;

        mTargetDir_int = mDir_int;
        mTargetDist_int = 0;

        mStopDist_int = INF_int;
// useless ?
//?        mStopSpeed_int = 0;
    }
////////

    // moves to a target given by absolute coordinates
// !!!! ???? TODO : int or float ? ???? !!!!
//?    public void absoluteTargetMove( int x, int y, boolean slowDown )
    public void absoluteTargetMove( float x, float y, boolean slowDown )
    {
        relativeTargetMove( x - mPosX, y - mPosY, slowDown );
    }
////////
    public void absoluteTargetMove_int( int x, int y, boolean slowDown )
    {
        relativeTargetMove_int( x - mPosX_int, y - mPosY_int, slowDown );
    }
////////

    // moves to a target given by relative coordinates
// !!!! ???? TODO : int or float ? ???? !!!!
//?    public void relativeTargetMove( int x, int y, boolean slowDown )
    public void relativeTargetMove( float x, float y, boolean slowDown )
    {
        mNewMove = true;
        mTarget = true;

        mTargetX = x;
        mTargetY = y;

// !!!! TODO : to remove (if using integers) !!!!
        Utils.roundSigned( mTargetX, PRECISION );
        Utils.roundSigned( mTargetY, PRECISION );

        mSlowDown = slowDown;

        // default values, may be changed if distance is "too short"
        if ( mSlowDown )
        {
            mStopDist = mDistanceFri;
        }
        else
        {
            mStopDist = 0.0f;
        }
        mStopSpeed = mMaxSpeed;
    }
////////
    public void relativeTargetMove_int( int x, int y, boolean slowDown )
    {
        mNewMove_int = true;
        mTarget_int = true;

        mTargetX_int = x;
        mTargetY_int = y;

        mSlowDown_int = slowDown;

        // default values, may be changed if distance is "too short"
        if ( mSlowDown_int )
        {
            mStopDist_int = mDistanceFri_int;
        }
        else
        {
            mStopDist_int = 0;
        }
        mStopSpeed_int = mMaxSpeed_int;
    }
////////

}
