//package ...;


public class Utils
{
    public float roundSigned( float value, int precision )
    {
        // return ( (int)( ( value * PRECISION ) + ( 0.5f * sign( value ) ) ) ) / PRECISION;
        float val = ( value * precision ) + ( 0.5f * sign( value ) );
        val = (int)val;
        return ( val / precision );
    }

    public float roundUnsigned( float value, int precision )
    {
        // return ( (int)( ( value * PRECISION ) + 0.5f ) ) / PRECISION;
        float val = ( value * precision ) + 0.5f;
        val = (int)val;
        return ( val / precision );
    }

// !!!! ???? TODO : is it better to reverse the order of the tests ? ???? !!!!
// => check if !=0 first ( cases more frequent ? )
// !!!! ???? TODO : OR, is it faster to do :
//        speed = (speedX * speedX) + (speedY * speedY);
//        if ( speed != 0 )
//            speed = sqrt( speed );
//    ?
    //public float computeLength( int deltaX, int deltaY )
    public float computeLength( float deltaX, float deltaY )
    {
        float length;

        if ( deltaX == 0.0f )
        {
            if ( deltaY == 0.0f )
            {
                length = 0.0f;
            }
            else if ( deltaY > 0.0f )
            {
                length = deltaY;
            }
            else // deltaY < 0.0f
            {
                length = -deltaY;
            }
        }
        else // deltaX != 0.0f
        {
            if ( deltaY == 0.0f )
            {
                if ( deltaX > 0.0f )
                {
                    length = deltaX;
                }
                else // deltaX < 0.0f
                {
                    length = -deltaX;
                }
            }
            else // deltaY != 0.0f
            {
// !!!! ???? TODO : what is the Math function for Sqrt ? ???? !!!!
                length = Math.sqrt( (deltaX * deltaX) + (deltaY * deltaY) );
// !!!! TODO : change/remove rounding to 2 decimals !!!!
                length = ( (int)( length * 100 ) ) / 100.0f;
            }
        }
        return length;
    }

// !!!! ???? TODO : is it better to reverse the order of the tests ? ???? !!!!
// => check if !=0 first ( cases more frequent ? )
    //public int computeAngle( int deltaX, int deltaY )
    public int computeAngle( float deltaX, float deltaY )
    {
        int angle;

        if ( (deltaX == 0.0f) && (deltaY == 0.0f) )
        {
            angle = -1; // no angle!
        }
        else
        {
            angle = (int) ( Math.toDegrees( Math.atan2( deltaY, deltaX ) ) );
            if ( angle < 0 )
            {
                angle += 360;
            }
        }
    }

// !!!! TODO : to be used if don't have "atan2" function !!!!
// => or if errors with atan on 360�
/*
    {
        if ( deltaX == 0.0f)
        {
            if ( deltaY == 0.0f )
            {
                angle = -1; // no angle!
            }
            else if ( deltaY > 0.0f )
            {
                angle = 90;
            }
            else // deltaY < 0.0f
            {
                angle = 270;
            }
        }
        else // deltaX != 0.0f
        {
            if ( deltaX > 0.0f )
            {
                if ( deltaY == 0.0f )
                {
                    angle = 0;
                }
                else // deltaY != 0.0f
                {
// !!!! TODO : find a way to avoid using "atan" !!!!
// !!!! ???? TODO : maybe no need to convert directions from radians to degrees ? ???? !!!!
// => can maybe do everything in radians ?
                    angle = (int) ( Math.toDegrees( Math.atan2( deltaY, deltaX ) ) );

                    if ( deltaY < 0.0f )
                    {
                        angle = 360 + angle;
                    }
                }
            }
            else // deltaX < 0.0f
            {
                if ( deltaY == 0.0f )
                {
                    angle = 180;
                }
                else // deltaY != 0.0f
                {
// !!!! TODO : find a way to avoid using "atan" !!!!
// !!!! ???? TODO : maybe no need to convert directions from radians to degrees ? ???? !!!!
// => can maybe do everything in radians ?
                    angle = (int) ( Math.toDegrees( Math.atan2( deltaY, deltaX ) ) );
                    angle = 180 + angle;
                }
            }

            if ( angle == 360 )
            {
                angle = 0;
            }
        }
        return angle;
    }
*/

}
