//package ...;


public class Utils
{
// !!!! ???? TODO : is it better to reverse the order of the tests ? ???? !!!!
// => check if !=0 first ( cases more frequent ? )
// !!!! ???? TODO : OR, is it faster to do :
//        speed = (speedX * speedX) + (speedY * speedY);
//        if ( speed != 0 )
//            speed = sqrt( speed );
//    ?
    public float computeLength( int deltaX, int deltaY )
    {
        int length;

        if ( deltaX == 0 )
        {
            if ( deltaY == 0 )
            {
                length = 0;
            }
            else if ( deltaY > 0 )
            {
                length = deltaY;
            }
            else // deltaY < 0
            {
                length = -deltaY;
            }
        }
        else // deltaX != 0
        {
            if ( deltaY == 0 )
            {
                if ( deltaX > 0 )
                {
                    length = deltaX;
                }
                else // deltaX < 0
                {
                    length = -deltaX;
                }
            }
            else // deltaY != 0
            {
// !!!! ???? TODO : what is the Math function for Sqrt ? ???? !!!!
                length = Math.sqrt( (deltaX * deltaX) + (deltaY * deltaY) );
            }
        }
        return length;
    }

// !!!! ???? TODO : is it better to reverse the order of the tests ? ???? !!!!
// => check if !=0 first ( cases more frequent ? )
    public int computeAngle( int deltaX, int deltaY )
    {
        int angle;

        if ( ( deltaX == 0 ) && ( deltaY == 0 ) )
        {
            angle = -1; // no angle!
        }
        else
        {
            angle = (int) ( Math.toDegrees( Math.atan2( deltaY, deltaX ) ) );
            if ( angle < 0 )
            {
                angle += 360;
            }
        }
    }

// !!!! TODO : to be used if don't have "atan2" function !!!!
// => or if errors with atan on 360�
/*
    {
        if ( deltaX == 0 )
        {
            if ( deltaY == 0 )
            {
                angle = -1; // no angle!
            }
            else if ( deltaY > 0 )
            {
                angle = 90;
            }
            else // deltaY < 0
            {
                angle = 270;
            }
        }
        else // deltaX != 0
        {
            if ( deltaX > 0 )
            {
                if ( deltaY == 0 )
                {
                    angle = 0;
                }
                else // deltaY != 0
                {
// !!!! TODO : find a way to avoid using "atan" !!!!
// !!!! ???? TODO : maybe no need to convert directions from radians to degrees ? ???? !!!!
// => can maybe do everything in radians ?
                    angle = (int) ( Math.toDegrees( Math.atan2( deltaY, deltaX ) ) );

                    if ( deltaY < 0 )
                    {
                        angle = 360 + angle;
                    }
                }
            }
            else // deltaX < 0
            {
                if ( deltaY == 0 )
                {
                    angle = 180;
                }
                else // deltaY != 0
                {
// !!!! TODO : find a way to avoid using "atan" !!!!
// !!!! ???? TODO : maybe no need to convert directions from radians to degrees ? ???? !!!!
// => can maybe do everything in radians ?
                    angle = (int) ( Math.toDegrees( Math.atan2( deltaY, deltaX ) ) );
                    angle = 180 + angle;
                }
            }

            if ( angle == 360 )
            {
                angle = 0;
            }
        }
        return angle;
    }
*/

}
