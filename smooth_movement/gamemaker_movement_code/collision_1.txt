
/*

 Collision handler : VERSION 1
 => called AFTER every object has "update" ( but NOT position ! )

!!!!
    This method is only applicable if have speeds "close" to each other !
    ( big differences imply loss of precision and heavy computations )
      - small integers : ideal case
        => e.g.: { 1, 2, 5, 10 }
      - numbers with same number of decimals : GOOD
        => e.g.: { 1, 2, 5 } or { 10, 20, 40, 50 } or { 0.1, 0.2, 0.5 } etc.
      - numbers with maximum 1 decimal difference : OK
        => e.g.: { 1.0, 2.5, 5.2 } or { 10.0, 20.5, 40.2, 50.0 } or { 0.1, 1.2, 2.5 } etc.
      - numbers with maximum 2 decimals difference : limit acceptable ( but not good )
        => e.g.: { 0.1, 2.0, 50.0 } or { 1.0, 20.0, 400.0 } or { 0.1, 0.2, 0.5 } etc.
      - numbers with maximum more than 2 decimals difference : not applicable
!!!!

  Algo:
  - have a determined "SPEED_MAX"
    ( maximum speed an object can go )
    ( double/float - can be int if int speeds )
    ( constant - hardcoded )

  - have a determined "SIZE_FACTOR"
    ( => maximum movement that can be done per step - movement without testing collision )
    ( => should be smaller than half the size of the smallest object )
    ( => must be such as : "SPEED_MAX / SIZE_FACTOR" is an integer )
    ( double/float - can be int if int speeds )
    ( constant - hardcoded )

  - determine "step_factor" => step_factor = SPEED_MAX / SIZE_FACTOR
    ( int )

  - set "time_left" => time_left = step_factor
    ( int )

  - while ( time_left > 0 )
    - move every object of speed / step_factor
    - for each pair of object ( that is "not collided" )
      ( only need to consider half of them, as its symmetrical => j > i )
      ( once have checked if pair is "collided" or not, can set it back to "not collided" )
      - determine distance between the 2 objects
      - if distance <= sum of their radii && moving towards each other ( => collision )
        - handle collision:
          - bounce: determine new speeds and directions for both
          - fuse: biggest absorbs smallest
          - explode : one explodes in several pieces ( with speeds and directions )
        - mark pair as "collided" ( don't consider in next iteration )
    - update time_left => time_left -= 1

*/


// !!!! TODO : handle "time_elapsed" !!!!
// => for now, only the "step" movement is handled ( time == 1 )
// => for that, replace "maxSpeed" with "maxSpeed * timeElapsed"
// !?!? => is it possible ????
// => will have problem of float values ... ( not an integer for "step_factor" )


// have a determined "SPEED_MAX"
SPEED_MAX = 20.0;
// have a determined "SIZE_FACTOR"
SIZE_FACTOR = 1.0;

//{


    int timeLeft;
    int stepFactor;

// !!!! TODO : should be declared somewhere else !!!!
// => should have a big array, able to hold variable number of objects
// ( => needs to be big enough )
    boolean collided[objectsList.size][objectsList.size];
// !!!! TODO : need to set all values to "false" !!!!
//...

    double deltaXSquared;
    double deltaYSquared;
    double sumRadiiSquared; 


    timeLeft = SPEED_MAX / SIZE_FACTOR;

    while ( timeLeft > 0 )
    {
        // move every object of speed/stepFactor
        foreach obj in objectsList
        {
// !!!! TODO : can precompute some values ... !!!!
// => could have a table with values for "speedX * (speed / stepFactor)" (and Y)
// ( ! => need to update the corresponding values when have a collision ! )
// !!!! TODO : posX and posY should be float/double values !!!!
            obj.posX += ( obj.speedX * ( obj.speed / stepFactor ) );
            obj.posY += ( obj.speedY * ( obj.speed / stepFactor ) );
        }

        // for each pair of object ( that is "not collided" )
        for ( i=1; i<objectsList.size; ++i )
        {
            for ( j=0; j<i; ++j )
            {
                if ( !collided[i][j] )
                {
// !!!! TODO : should use local variables to store current objects, instead of using "objectsList[i]" !!!!
                    // determine distance between the 2 objects
                    deltaXSquared = objectsList[i].x - objectsList[j].x;
                    deltaXSquared *= deltaXSquared;
                    deltaYSquared = objectsList[i].y - objectsList[j].y;
                    deltaYSquared *= deltaYSquared;

                    sumRadiiSquared = objectsList[i].radius + objectsList[j].radius;
                    sumRadiiSquared *= sumRadiiSquared;

                    // if distance <= sum of their radii
                    if ( deltaXSquared + deltaYSquared <= sumRadiiSquared )
                    {
// !!!! ???? TODO : should check that moving towards each other ? ???? !!!!
// => implies a dot product, etc => is it worth it ?
                        // moving towards each other ( => collision )
                        //if ( ... )
                        //{

                        // handle collision depending on type of objects
                        // ( bounce, fuse, explode )
                        // bounce: determine new speeds and directions for both
                        // fuse: biggest absorbs smallest
                        // explode: one explodes in several pieces ( with speeds and directions )
                        handleCollision ( objectsList[i], objectsList[j] );
// !!!! TODO : need to update precomputed values !!!!

// !!!! TODO : careful if have an object disappearing and/or new objects created !!!!
// => in handle collision, if an object "dies", it should call "object.die",
//    which will mark the object as "dead"
// ? => object should also be removed from objectsList ! BUT careful that doesn't cause problems !
// !!!! ???? TODO : OK to ignore newly created objects in the whole step ? ???? !!!!
//...

                        //if ( !objectsList[i].dead && !objectsList[j].dead )
                        //{

                        // mark pair as "collided" ( don't consider it in next iteration )
                        collided[i][j] = true;

                        //}

                        //}
                    }
                }
                else
                {
                    collided[i][j] = false; // reset collision for next iteration
                }
            }
        }

        // update timeLeft
         timeLeft -= 1;

    }

//}
